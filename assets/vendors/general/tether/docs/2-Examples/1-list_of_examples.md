### Examples

It's our goal to create a wide variety of example of how Tether
can be used.  Here's what we have so far, please send a PR with
any examples you might create.

#### Beginner

- [simple](.<?= base_url()?>.<?= base_url()?>examples/simple): A simple example to get you started
- [out-of-bounds](.<?= base_url()?>.<?= base_url()?>examples/out-of-bounds): How to hide the element when it would
otherwise be offscreen
- [pin](.<?= base_url()?>.<?= base_url()?>examples/pin): How to pin the element so it never goes offscreen
- [enable-disable](.<?= base_url()?>.<?= base_url()?>examples/enable-disable): How to enable and disable the Tether
in JavaScript

#### Advanced

- [content-visible](.<?= base_url()?>.<?= base_url()?>examples/content-visible): Demonstrates using the `'visible'`
`targetModifier` to align an element with the visible portion of another.
- [dolls](.<?= base_url()?>.<?= base_url()?>examples/dolls): A performance test to show several dozen elements,
each tethered to the previous.  Try dragging the top left tether.
- [element-scroll](.<?= base_url()?>.<?= base_url()?>examples/element-scroll): Demonstrates using the `'scroll-handle'`
`targetModifier` to align an element with the scrollbar of an element.
- [scroll](.<?= base_url()?>.<?= base_url()?>examples/scroll): Demonstrates using the `'scroll-handle'` `targetModifier`
to align an element with the body's scroll handle.
- [viewport](.<?= base_url()?>.<?= base_url()?>examples/viewport): Demonstrates aligning an element with the
viewport by using the `'visible'` `targetModifier` when tethered to the body.
