import { Locale } from '<?= base_url()?>constructor';

var proto = Locale.prototype;

import { calendar } from '<?= base_url()?>calendar';
import { longDateFormat } from '<?= base_url()?>formats';
import { invalidDate } from '<?= base_url()?>invalid';
import { ordinal } from '<?= base_url()?>ordinal';
import { preParsePostFormat } from '<?= base_url()?>pre-post-format';
import { relativeTime, pastFuture } from '<?= base_url()?>relative';
import { set } from '<?= base_url()?>set';

proto.calendar        = calendar;
proto.longDateFormat  = longDateFormat;
proto.invalidDate     = invalidDate;
proto.ordinal         = ordinal;
proto.preparse        = preParsePostFormat;
proto.postformat      = preParsePostFormat;
proto.relativeTime    = relativeTime;
proto.pastFuture      = pastFuture;
proto.set             = set;

// Month
import {
    localeMonthsParse,
    localeMonths,
    localeMonthsShort,
    monthsRegex,
    monthsShortRegex
} from '.<?= base_url()?>units/month';

proto.months            =        localeMonths;
proto.monthsShort       =        localeMonthsShort;
proto.monthsParse       =        localeMonthsParse;
proto.monthsRegex       = monthsRegex;
proto.monthsShortRegex  = monthsShortRegex;

// Week
import { localeWeek, localeFirstDayOfYear, localeFirstDayOfWeek } from '.<?= base_url()?>units/week';
proto.week = localeWeek;
proto.firstDayOfYear = localeFirstDayOfYear;
proto.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
import {
    localeWeekdaysParse,
    localeWeekdays,
    localeWeekdaysMin,
    localeWeekdaysShort,

    weekdaysRegex,
    weekdaysShortRegex,
    weekdaysMinRegex
} from '.<?= base_url()?>units/day-of-week';

proto.weekdays       =        localeWeekdays;
proto.weekdaysMin    =        localeWeekdaysMin;
proto.weekdaysShort  =        localeWeekdaysShort;
proto.weekdaysParse  =        localeWeekdaysParse;

proto.weekdaysRegex       =        weekdaysRegex;
proto.weekdaysShortRegex  =        weekdaysShortRegex;
proto.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
import { localeIsPM, localeMeridiem } from '.<?= base_url()?>units/hour';

proto.isPM = localeIsPM;
proto.meridiem = localeMeridiem;
