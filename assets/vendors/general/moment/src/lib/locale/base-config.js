import { defaultCalendar } from '<?= base_url()?>calendar';
import { defaultLongDateFormat } from '<?= base_url()?>formats';
import { defaultInvalidDate } from '<?= base_url()?>invalid';
import { defaultOrdinal, defaultDayOfMonthOrdinalParse } from '<?= base_url()?>ordinal';
import { defaultRelativeTime } from '<?= base_url()?>relative';

// months
import {
    defaultLocaleMonths,
    defaultLocaleMonthsShort,
} from '.<?= base_url()?>units/month';

// week
import { defaultLocaleWeek } from '.<?= base_url()?>units/week';

// weekdays
import {
    defaultLocaleWeekdays,
    defaultLocaleWeekdaysMin,
    defaultLocaleWeekdaysShort,
} from '.<?= base_url()?>units/day-of-week';

// meridiem
import { defaultLocaleMeridiemParse } from '.<?= base_url()?>units/hour';

export var baseConfig = {
    calendar: defaultCalendar,
    longDateFormat: defaultLongDateFormat,
    invalidDate: defaultInvalidDate,
    ordinal: defaultOrdinal,
    dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
    relativeTime: defaultRelativeTime,

    months: defaultLocaleMonths,
    monthsShort: defaultLocaleMonthsShort,

    week: defaultLocaleWeek,

    weekdays: defaultLocaleWeekdays,
    weekdaysMin: defaultLocaleWeekdaysMin,
    weekdaysShort: defaultLocaleWeekdaysShort,

    meridiemParse: defaultLocaleMeridiemParse
};
