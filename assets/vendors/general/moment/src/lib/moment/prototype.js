import { Moment } from '<?= base_url()?>constructor';

var proto = Moment.prototype;

import { add, subtract } from '<?= base_url()?>add-subtract';
import { calendar, getCalendarFormat } from '<?= base_url()?>calendar';
import { clone } from '<?= base_url()?>clone';
import { isBefore, isBetween, isSame, isAfter, isSameOrAfter, isSameOrBefore } from '<?= base_url()?>compare';
import { diff } from '<?= base_url()?>diff';
import { format, toString, toISOString, inspect } from '<?= base_url()?>format';
import { from, fromNow } from '<?= base_url()?>from';
import { to, toNow } from '<?= base_url()?>to';
import { stringGet, stringSet } from '<?= base_url()?>get-set';
import { locale, localeData, lang } from '<?= base_url()?>locale';
import { prototypeMin, prototypeMax } from '<?= base_url()?>min-max';
import { startOf, endOf } from '<?= base_url()?>start-end-of';
import { valueOf, toDate, toArray, toObject, toJSON, unix } from '<?= base_url()?>to-type';
import { isValid, parsingFlags, invalidAt } from '<?= base_url()?>valid';
import { creationData } from '<?= base_url()?>creation-data';

proto.add               = add;
proto.calendar          = calendar;
proto.clone             = clone;
proto.diff              = diff;
proto.endOf             = endOf;
proto.format            = format;
proto.from              = from;
proto.fromNow           = fromNow;
proto.to                = to;
proto.toNow             = toNow;
proto.get               = stringGet;
proto.invalidAt         = invalidAt;
proto.isAfter           = isAfter;
proto.isBefore          = isBefore;
proto.isBetween         = isBetween;
proto.isSame            = isSame;
proto.isSameOrAfter     = isSameOrAfter;
proto.isSameOrBefore    = isSameOrBefore;
proto.isValid           = isValid;
proto.lang              = lang;
proto.locale            = locale;
proto.localeData        = localeData;
proto.max               = prototypeMax;
proto.min               = prototypeMin;
proto.parsingFlags      = parsingFlags;
proto.set               = stringSet;
proto.startOf           = startOf;
proto.subtract          = subtract;
proto.toArray           = toArray;
proto.toObject          = toObject;
proto.toDate            = toDate;
proto.toISOString       = toISOString;
proto.inspect           = inspect;
proto.toJSON            = toJSON;
proto.toString          = toString;
proto.unix              = unix;
proto.valueOf           = valueOf;
proto.creationData      = creationData;

// Year
import { getSetYear, getIsLeapYear } from '.<?= base_url()?>units/year';
proto.year       = getSetYear;
proto.isLeapYear = getIsLeapYear;

// Week Year
import { getSetWeekYear, getSetISOWeekYear, getWeeksInYear, getISOWeeksInYear } from '.<?= base_url()?>units/week-year';
proto.weekYear    = getSetWeekYear;
proto.isoWeekYear = getSetISOWeekYear;

// Quarter
import { getSetQuarter } from '.<?= base_url()?>units/quarter';
proto.quarter = proto.quarters = getSetQuarter;

// Month
import { getSetMonth, getDaysInMonth } from '.<?= base_url()?>units/month';
proto.month       = getSetMonth;
proto.daysInMonth = getDaysInMonth;

// Week
import { getSetWeek, getSetISOWeek } from '.<?= base_url()?>units/week';
proto.week           = proto.weeks        = getSetWeek;
proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
proto.weeksInYear    = getWeeksInYear;
proto.isoWeeksInYear = getISOWeeksInYear;

// Day
import { getSetDayOfMonth } from '.<?= base_url()?>units/day-of-month';
import { getSetDayOfWeek, getSetISODayOfWeek, getSetLocaleDayOfWeek } from '.<?= base_url()?>units/day-of-week';
import { getSetDayOfYear } from '.<?= base_url()?>units/day-of-year';
proto.date       = getSetDayOfMonth;
proto.day        = proto.days             = getSetDayOfWeek;
proto.weekday    = getSetLocaleDayOfWeek;
proto.isoWeekday = getSetISODayOfWeek;
proto.dayOfYear  = getSetDayOfYear;

// Hour
import { getSetHour } from '.<?= base_url()?>units/hour';
proto.hour = proto.hours = getSetHour;

// Minute
import { getSetMinute } from '.<?= base_url()?>units/minute';
proto.minute = proto.minutes = getSetMinute;

// Second
import { getSetSecond } from '.<?= base_url()?>units/second';
proto.second = proto.seconds = getSetSecond;

// Millisecond
import { getSetMillisecond } from '.<?= base_url()?>units/millisecond';
proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
import {
    getSetOffset,
    setOffsetToUTC,
    setOffsetToLocal,
    setOffsetToParsedOffset,
    hasAlignedHourOffset,
    isDaylightSavingTime,
    isDaylightSavingTimeShifted,
    getSetZone,
    isLocal,
    isUtcOffset,
    isUtc
} from '.<?= base_url()?>units/offset';
proto.utcOffset            = getSetOffset;
proto.utc                  = setOffsetToUTC;
proto.local                = setOffsetToLocal;
proto.parseZone            = setOffsetToParsedOffset;
proto.hasAlignedHourOffset = hasAlignedHourOffset;
proto.isDST                = isDaylightSavingTime;
proto.isLocal              = isLocal;
proto.isUtcOffset          = isUtcOffset;
proto.isUtc                = isUtc;
proto.isUTC                = isUtc;

// Timezone
import { getZoneAbbr, getZoneName } from '.<?= base_url()?>units/timezone';
proto.zoneAbbr = getZoneAbbr;
proto.zoneName = getZoneName;

// Deprecations
import { deprecate } from '.<?= base_url()?>utils/deprecate';
proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

export default proto;
