import { createDuration } from '.<?= base_url()?>duration/create';
import { createLocal } from '.<?= base_url()?>create/local';
import { isMoment } from '.<?= base_url()?>moment/constructor';

export function to (time, withoutSuffix) {
    if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
             createLocal(time).isValid())) {
        return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
    } else {
        return this.localeData().invalidDate();
    }
}

export function toNow (withoutSuffix) {
    return this.to(createLocal(), withoutSuffix);
}
