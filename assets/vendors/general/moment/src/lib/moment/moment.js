import { createLocal } from '.<?= base_url()?>create/local';
import { createUTC } from '.<?= base_url()?>create/utc';
import { createInvalid } from '.<?= base_url()?>create/valid';
import { isMoment } from '<?= base_url()?>constructor';
import { min, max } from '<?= base_url()?>min-max';
import { now } from '<?= base_url()?>now';
import momentPrototype from '<?= base_url()?>prototype';

function createUnix (input) {
    return createLocal(input * 1000);
}

function createInZone () {
    return createLocal.apply(null, arguments).parseZone();
}

export {
    now,
    min,
    max,
    isMoment,
    createUTC,
    createUnix,
    createLocal,
    createInZone,
    createInvalid,
    momentPrototype
};
