import { isValid as _isValid } from '.<?= base_url()?>create/valid';
import extend from '.<?= base_url()?>utils/extend';
import getParsingFlags from '.<?= base_url()?>create/parsing-flags';

export function isValid () {
    return _isValid(this);
}

export function parsingFlags () {
    return extend({}, getParsingFlags(this));
}

export function invalidAt () {
    return getParsingFlags(this).overflow;
}
