import { Moment } from '<?= base_url()?>constructor';

export function clone () {
    return new Moment(this);
}
