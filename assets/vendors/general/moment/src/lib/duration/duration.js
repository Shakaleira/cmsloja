// Side effect imports
import '<?= base_url()?>prototype';

import { createDuration } from '<?= base_url()?>create';
import { isDuration } from '<?= base_url()?>constructor';
import {
    getSetRelativeTimeRounding,
    getSetRelativeTimeThreshold
} from '<?= base_url()?>humanize';

export {
    createDuration,
    isDuration,
    getSetRelativeTimeRounding,
    getSetRelativeTimeThreshold
};
