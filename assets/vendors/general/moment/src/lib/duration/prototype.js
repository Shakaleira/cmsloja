import { Duration } from '<?= base_url()?>constructor';

var proto = Duration.prototype;

import { abs } from '<?= base_url()?>abs';
import { add, subtract } from '<?= base_url()?>add-subtract';
import { as, asMilliseconds, asSeconds, asMinutes, asHours, asDays, asWeeks, asMonths, asQuarters, asYears, valueOf } from '<?= base_url()?>as';
import { bubble } from '<?= base_url()?>bubble';
import { clone } from '<?= base_url()?>clone';
import { get, milliseconds, seconds, minutes, hours, days, months, years, weeks } from '<?= base_url()?>get';
import { humanize } from '<?= base_url()?>humanize';
import { toISOString } from '<?= base_url()?>iso-string';
import { lang, locale, localeData } from '.<?= base_url()?>moment/locale';
import { isValid } from '<?= base_url()?>valid';

proto.isValid        = isValid;
proto.abs            = abs;
proto.add            = add;
proto.subtract       = subtract;
proto.as             = as;
proto.asMilliseconds = asMilliseconds;
proto.asSeconds      = asSeconds;
proto.asMinutes      = asMinutes;
proto.asHours        = asHours;
proto.asDays         = asDays;
proto.asWeeks        = asWeeks;
proto.asMonths       = asMonths;
proto.asQuarters     = asQuarters;
proto.asYears        = asYears;
proto.valueOf        = valueOf;
proto._bubble        = bubble;
proto.clone          = clone;
proto.get            = get;
proto.milliseconds   = milliseconds;
proto.seconds        = seconds;
proto.minutes        = minutes;
proto.hours          = hours;
proto.days           = days;
proto.weeks          = weeks;
proto.months         = months;
proto.years          = years;
proto.humanize       = humanize;
proto.toISOString    = toISOString;
proto.toString       = toISOString;
proto.toJSON         = toISOString;
proto.locale         = locale;
proto.localeData     = localeData;

// Deprecations
import { deprecate } from '.<?= base_url()?>utils/deprecate';

proto.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString);
proto.lang = lang;
