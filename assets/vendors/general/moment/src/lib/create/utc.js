import { createLocalOrUTC } from '<?= base_url()?>from-anything';

export function createUTC (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, true).utc();
}
