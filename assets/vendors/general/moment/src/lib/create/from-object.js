import { normalizeObjectUnits } from '.<?= base_url()?>units/aliases';
import { configFromArray } from '<?= base_url()?>from-array';
import map from '.<?= base_url()?>utils/map';

export function configFromObject(config) {
    if (config._d) {
        return;
    }

    var i = normalizeObjectUnits(config._i);
    config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
        return obj && parseInt(obj, 10);
    });

    configFromArray(config);
}
