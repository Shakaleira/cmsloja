import { createLocalOrUTC } from '<?= base_url()?>from-anything';

export function createLocal (input, format, locale, strict) {
    return createLocalOrUTC(input, format, locale, strict, false);
}
