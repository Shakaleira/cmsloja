$('document').ready(function(){

    selects_combo('.select2');

    mascaras('.valor');

    $('#dropzone_cadastro').dropzone({
        url: base_url+"/configuracoes/upload", // Set the url for your upload script location
        method:'post',
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        addRemoveLinks: true,
        acceptedFiles: ".jpeg,.jpg,.png,.gif",
        init: function() {
            var imagens = [];
            this.on("complete", function (file) {
                var novo_arquivo = JSON.parse(file.xhr.response);
                imagens.push(novo_arquivo.msg);
                var itens_atuais =  $('.input_image').val();
                if(itens_atuais !==  '') {
                    imagens.push(itens_atuais);
                }
                var filtered = imagens.filter(function(el) { return el; });
                var new_filter = remove_equal(filtered);

                $('.input_image').val(new_filter);

            });
            this.on("maxfilesexceeded", function(file) {
                // this.removeAllFiles();
                // this.addFile(file);
            });
        },
        accept: function(file, done) {
            if (file.name == "justinbieber.jpg") {
                done("Naha, you don't.");
            } else {
                done();
            }
        }
    });


    var KTBootstrapSelect = function () {
        // Private functions
        var demos = function () {
            // minimum setup
            $('.kt-selectpicker').selectpicker();
        }

        return {
            // public functions
            init: function() {
                demos();
            }
        };
    }();

    jQuery(document).ready(function() {
        KTBootstrapSelect.init();
    });

    // ações de controle produto
    $('.selectpicker_produto_id').selectpicker('refresh');
    $(document).on('change', 'select[name=produto_id]', function (e) {
        e.preventDefault();
        var elemento = $(this);
        $.ajax({
            url: base_url+'/produtos/productByQtd',
            method : 'POST',
            data: {'data' : elemento.val()},
            beforeSend: function (){
                //$('.render_produto_id').empty().append('<center><progress></progress></center>');
            },
            success: function(res) {
                $('.render_produto_id').append($("#estoque").render(res));
            },
            error: function(res) {
                console.log(res);
            }
        });
        return false;
    });



    if(pathArray.includes("condicional")){
        var id_orcamento = $('input[name=id]').val();
        //
        // Produtos Condicional
        //
        if (id_orcamento === undefined || id_orcamento === null) {
            return true;
        }
        $.ajax({
            url: base_url+'/produtos/productByConditional',
            method : 'POST',
            data: {'produto_id':id_orcamento},
            beforeSend: function (){
                $('.render_produto_id').empty().append('<center><progress></progress></center>');
            },
            success: function(res) {
                $('.render_produto_id').empty().append($("#estoque").render(res));
                /////////////////////
                // acao de remover item de orçamento//
                ////////////////////
                $('.deletar_registro').click(function(e){
                    e.preventDefault();
                    var elemento = $(this);
                    $('.id_produto_'+elemento.data('id')).remove();
                    var condicional = $('input[name=id]').val();
                    $.ajax({
                        url: base_url+'/produtos/atualizaItempProduto',
                        method : 'POST',
                        data: {
                            'produto_id':elemento.data('id'),
                            'quantidade':elemento.data('quantidade'),
                            'condicional': condicional
                        },
                        success: function(res) {
                            swal.fire({
                                title: 'Deletado!',
                                text: 'Ação Concluida com Sucesso!',
                                type: 'success',
                                buttonsStyling: false,
                            });
                            location.reload();
                        },
                        error: function(res) {
                            console.log(res);
                        }
                    });
                    return false;
                });
            },
            error: function(res) {
                console.log(res);
            }
        });

    }


    $(document).on('click', '.deletar_imagem', function () {
        var elemento = $(this);
        var modelo = $('input[name=modelo]').val();
        $.ajax({
            url: base_url+'Cadastros/deleteImage',
            method : 'POST',
            data: {
                'produto' : elemento.data('produto'),
                'posicao' :  elemento.data('posicao'),
                'campo_nome' :  elemento.data('campo_nome'),
                'modelo' :  modelo
            },
            beforeSend: function (){
                $('.carregamento').empty().append('<center><progress></progress></center>');
            },
            success: function(res) {
                $('.carregamento').empty();
                $('.imagem_'+elemento.data('posicao')).hide();
                var itens_atuais =  $('.input_image').val();
                $('.input_image').val(itens_atuais+','+res);
                location.reload();
            },
            error: function(res) {
                console.log(res);
            }
        });
    });
});

function printDiv(printpage) {
    var headstr = "<html><head><title>Impressao</title><link href="+base_url+"'assets/css/demo7/style.bundle.css' rel='stylesheet' type='text/css' /></head><body>";
    var footstr = "</body>";
    var newstr = document.all.item(printpage).innerHTML;
    var oldstr = document.body.innerHTML;
    document.body.innerHTML = headstr + newstr  + footstr;
    window.print();
    document.body.innerHTML = oldstr;
    return false;
}

//submit do form
$(document).on("submit", "form", function(){
    var base = $('input[name=base]').val();
    var modelo = $('input[name=modelo]').val();
    var comando = $('input[name=comando]').val();
    var dados = $(this).serialize();
    $.ajax({
        url: base_url+base+'/'+modelo+'/'+comando,
        type : 'POST',
        dataType:'json',
        data: dados,
        beforeSend: function (){
            swal.showLoading();
        },
        success: function(msg) {
            swal.close();
            if(msg.type === 'error'){
                Command: toastr["error"](msg.msg, "Erro");
            }else{
                Command: toastr["success"](msg.msg, "Sucesso");
            }
        },
        error: function(res) {
            console.log(res);
        }
    });
    return false;
});

$(".spectrum_color").spectrum();

$('.more_variation').click(function(e){
   e.preventDefault();
    $.ajax({
        url: base_url+'/Cadastros/retrieveVariationsProduct',
        type : 'POST',
        dataType:'json',
        beforeSend: function (){
            swal.showLoading();
        },
        success: function(retorno) {
            swal.close();
            var contador =  $('.itens_clonados').length;
            if(contador >= 0){
                $('.less_variation ').addClass('svg-icon-primary');
            }
            $('.base_clone_variations').append($("#variacoes_produto").render(retorno));
            $('.select2clone').select2({
                placeholder: "Selecione um Item"
            });
            mascaras('.valor');

            $('.remove_cloned_item').click(function(){
                var contador =  $('.itens_clonados').length;
                $(this).parent().parent().parent().remove();
                if(contador < 0){
                    $('.less_variation ').removeClass('svg-icon-primary');
                }
            });
        },
        error: function(res) {
            console.log(res);
        }
    });

   return false;
});

$(document).on('click', '.remove_variacao', function (e) {
    e.preventDefault();
    var elemento = $(this);
    $.ajax({
        url: base_url+'/Cadastros/deleteVariation',
        method : 'POST',
        data: {'id' : elemento.data('id')},
        beforeSend: function (){
            swal.showLoading();
        },
        success: function(res) {
            swal.close();
            Command: toastr["success"]('Removido com sucesso', "Sucesso");
            $('.variacoes_id_'+elemento.data('id')).remove();
        },
        error: function(res) {
            Command: toastr["error"]('Erro', "Erro");
        }
    });
    return false;
});

