"use strict";
var base_url;
var host;
var pathArray;

if(ambiente.includes('localhost')){
     base_url = window.location.origin+'/cmsloja/';  // Variavel que CONTEM O HOST com http:
}else{
     base_url = window.location.origin+'/';  // Variavel que CONTEM O HOST com http:
}
host = window.location.host;
pathArray = window.location.pathname.split( '/' ); // Variavel que contem os PARAMETROS DA URL

function dataFormatPT(){
    $('#data1').datepicker({ language: "pt-BR", format: "dd/mm/yyyy"});
}

function remove_equal(x){
    x = x.join(',');
    var arr = x.split(',');
    x = arr.filter(function(value, index, self) {
        return self.indexOf(value) === index;
    }).join(',');
    return x;
}

function upload(id,path,tag_append,file_size = 2){
    $('#'+id).dropzone({
        url: base_url+""+path+"", // Set the url for your upload script location
        method:'post',
        paramName: "file", // The name that will be used to transfer the file
        maxFilesize: 2, // MB
        addRemoveLinks: true,
        init: function() {
            var imagens = [];
            this.on("complete", function (file) {
                var novo_arquivo = JSON.parse(file.xhr.response);
                imagens.push(novo_arquivo.msg);
                var itens_atuais =  $(tag_append).val();
                if(itens_atuais !==  '') {
                    imagens.push(itens_atuais);
                }
                var filtered = imagens.filter(function(el) { return el; });
                var new_filter = remove_equal(filtered);

                $(tag_append).val(new_filter);

            });
            this.on("maxfilesexceeded", function(file) {
                // this.removeAllFiles();
                // this.addFile(file);
            });
            this.on('error', function(file, response) {
                console.log(response);
            });
        },
        accept: function(file, done) {
        }
    });
}


$('.remove_thumb').click(function(e){
    e.preventDefault();
    var campo = $(this).data('campo');
    var position = $(this).data('position');
    $.ajax({
        url: base_url + '/Configuracoes/removeItemConf',
        cache: false,
        method: 'POST',
        data: {
            'campo' : campo,
            'position': position
        },
        dataType: 'json',
        error: function(error) {
            Command: toastr["error"](error, "Erro");
        },
        beforeSend: function() {
            swal.showLoading();
        },
        success: function(data) {
            if (data.msg === true) {
                Command: toastr["success"]("Ação concluida!", "Sucesso");
                location.reload();
            }
        }
    });
    return false;
});

function selects_combo(campo){
    $(campo).select2({
        placeholder: "Selecione um Item"
    });
}

function RemoveAccents(str) {
    var accents    = 'ÀÁÂÃÄÅàáâãäåÒÓÔÕÕÖØòóôõöøÈÉÊËèéêëðÇçÐÌÍÎÏìíîïÙÚÛÜùúûüÑñŠšŸÿýŽž';
    var accentsOut = "AAAAAAaaaaaaOOOOOOOooooooEEEEeeeeeCcDIIIIiiiiUUUUuuuuNnSsYyyZz";
    str = str.split('');
    var strLen = str.length;
    var i, x;
    for (i = 0; i < strLen; i++) {
        if ((x = accents.indexOf(str[i])) != -1) {
            str[i] = accentsOut[x];
        }
    }
    return str.join('');
}
