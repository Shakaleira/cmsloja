jQuery(document).ready(function () {

    $("#cpfcnpj").keydown(function(){
        try {
            $("#cpfcnpj").unmask();
        } catch (e) {}

        var tamanho = $("#cpfcnpj").val().length;

        if(tamanho < 11){
            $("#cpfcnpj").mask("999.999.999-99");
        } else {
            $("#cpfcnpj").mask("99.999.999/9999-99");
        }

        // ajustando foco
        var elem = this;
        setTimeout(function(){
            // mudo a posição do seletor
            elem.selectionStart = elem.selectionEnd = 10000;
        }, 0);
        // reaplico o valor para mudar o foco
        var currentValue = $(this).val();
        $(this).val('');
        $(this).val(currentValue);
    });

    $('.kt-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            url: base_url+'login/autenticaUsuarioTrial',
            type:"POST",
            data:$(this).serialize(),
            dataType : "json",
            beforeSend: function(){
                swal.fire({
                    title: "Enviando...",
                    text: "Aguarde um momento",
                    imageUrl: "images/ajaxloader.gif",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
            },
            success: function(response){
                if(response.logado == false){
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: response.message,
                    });
                }else{
                    window.location.href = base_url+'trial';
                }
            }
        });
        return false;
    });


    $('#limit_hold').keyup(function (){
        var input = $(this).val();
        var input_trait = input.replace(/[^A-Z0-9]/ig, "_");
        $(this).val(input_trait);
    });

    jQuery("input.telefone")
    .mask("(99) 9999-999999")
    .focusout(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        phone = target.value.replace(/\D/g, '');
        element = $(target);
        element.unmask();
        if(phone.length > 10) {
            element.mask("(99) 99999-99999");
        } else {
            element.mask("(99) 9999-999999");
        }
    });

});

