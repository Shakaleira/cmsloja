$('document').ready(function(){


    $('.kt-datatable').KTDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    url: base_url+'/configuracoes/usuarios_datatable',
                },
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            autoColumns: true,
        },
        layout: {
            scroll: true,
            footer: false,
        },
        sortable: true,
        pagination: true,
        search: {
            input: $('#generalSearch'),
        },
        translate: {
            records:{
                noRecords: 'Nenhum dado Localizado',
                processing: 'Aguarde Processando...'
            },
            toolbar:{
                pagination:{
                    items:{
                        info: 'Exibindo {{start}} - {{end}} de {{total}} registros'
                    }
                }
            }
        },
        columns:[
            {
                field: 'id',
                title: 'ID',
            },
            {
                field: 'nome',
                title: 'nome',
            },
            {
                field: 'email',
                title: 'Email',
            },
            {
                field: 'nivel',
                title: 'Nivel',
            },
            {
                field: 'Actions',
                title: 'Ações',
                sortable: false,
                width: 130,
                overflow: 'visible',
                textAlign: 'center',
                template: function (row, index, datatable) {
                    $('.deletar_registro').click(function (e) {
                        e.preventDefault();
                        Swal.fire({
                            title: 'Confirmação de Ação.',
                            text: "Certeza que deseja inativar este item?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonText: 'Cancelar',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, excluir!'
                        }).then((result) => {
                            if (result.value) {
                                $.ajax({
                                    url: base_url+'/configuracoes/remove_usuario',
                                    cache:false,
                                    method:'POST',
                                    data:{'id':id_referencia},
                                    dataType:'json',
                                    error: function (error) {
                                        alert('Falha ao remover');
                                    },
                                    success: function (data) {
                                        Swal.fire(
                                            'Inativado!',
                                            'Registro Inativado',
                                            'success'
                                        )
                                    }
                                })
                            }
                        })
                        return false;
                    })

                    var html = '<div class="bloco_acoes"><a href="'+base_url+'/configuracoes/usuarios/'+row.id+'" data-id="'+row.id+'" class="btn btn-hover-brand btn-icon btn-pill" title="Edit details"><i class="la la-edit"></i></a><a data-id="'+row.id+'" class="deletar_registro btn btn-hover-danger btn-icon btn-pill" title="Delete"><i class="la la-trash"></i></a></div>';
                    return html;
                }
            }
        ],
        rows:{
            autoHide: false
        }

    });

    $('#dropzone').dropzone({
        url: base_url+"/configuracoes/upload", // Set the url for your upload script location
        method:'post',
        paramName: "file", // The name that will be used to transfer the file
        maxFiles: 1,
        maxFilesize: 5, // MB
        addRemoveLinks: true,
        init: function() {
            this.on("complete", function (file) {
                var novo_arquivo = JSON.parse(file.xhr.response);
                $('input[name=imagem]').val(novo_arquivo.msg);
            });
            this.on("maxfilesexceeded", function(file) {
                this.removeAllFiles();
                this.addFile(file);
            });
        },
        accept: function(file, done) {
            if (file.name == "justinbieber.jpg") {
                done("Naha, you don't.");
            } else {
                done();
            }
        }
    });


});