$('document').ready(function(){

    $('.btn-quickview').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            url: '../quickview',
            method:'POST',
            data:{'id':id},
            dataType:'json',
            beforeSend: function(){

            },
            success: function (data) {

                $('.render_produto').empty().append($("#quickview").render(data));
                var html =  $('#render_produto').html();
                var options = {
                    message: html,
                    title: '<b>'+data.nome.toUpperCase()+'</b>',
                    size: 'lg',
                    useBin: true
                };
                eModal.alert(options);

            },
            complete: function (){

            }
        });
    });

    $('.remove_item').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $.ajax({
            url: '../removecart',
            method:'POST',
            data:{'id':id},
            dataType:'json',
            success: function () {
                document.location.reload(true);
            },
            complete: function (){
            }
        });
    });

    $('.finalizar_carrinho').click(function(e){
        e.preventDefault();
        $('.hidden_form').show();
        $('.conteudo_carrinho').hide();
        $('.orcamento_carrinho').submit(function(e){
            e.preventDefault();
            var dados = $(this).serialize();
            $.ajax({
                url: '../LojaController/sendEmail',
                method:'POST',
                data: dados,
                dataType:'json',
                beforeSend:function(){
                    $('.hidden_form').hide();
                    $('.loader').empty().append('<progress></progress>');
                },
                success: function (response) {
                    $('.loader').empty().append(response.msg);
                    setTimeout(function(){
                        window.location.reload(1);
                    }, 5000);
                }
            });
            return false;
        });

        $('.voltar').click(function(){
            $('.hidden_form').hide();
            $('.conteudo_carrinho').show();
        });

        return false;
    });

    $('.formulario_contato').submit(function(e){
        e.preventDefault();
        var dados = $(this).serialize();
        $.ajax({
            url: base_url+'LojaController/sendcontato',
            method:'POST',
            data: dados,
            dataType:'json',
            beforeSend: function(){
                $('.loader').append('<progress></progress>');
            },
            success: function () {
                $('.loader').empty();
                alert('Contato Enviado com sucesso!');
            }
        });
        return false;
    });

    $('.select2').select2();

    $('.busca_geral').submit(function (e){
        e.preventDefault();
        var category = $("select[name='category']",this).val();
        var search = $("input[name='search']",this).val();
        var base_path = $("input[name='base_path']",this).val();
        var caminho = '';
        search = RemoveAccents(search);
        if(search === ''){
            caminho = base_path+'categoria/'+category+'/busca';
        }else{
            caminho = base_path+'categoria/'+category+'/busca/'+search;
        }
        window.location.href = caminho;
    });


});