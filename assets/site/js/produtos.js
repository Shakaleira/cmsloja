$('document').ready(function(){

    $('.variacao_cor').click(function(e){
        e.preventDefault();
        var id_produto = $('input[name=id_produto]').val();
        var color = $(this).data('color');
        $.ajax({
            url: base_url+'LojaController/variationsTamanho',
            method:'POST',
            data:{'id_produto': id_produto, 'color' : color},
            dataType:'json',
            beforeSend: function(){
                $('.append_valores').empty();
                $('.append_tamanhos').empty().append('<progress></progress>');
            },
            success: function (data) {
                $('.append_tamanhos').empty().append($("#tamanho_variacao").render({'tamanhos': data}));
                tamanho_vars();
                $('.tamanho_vars').first().trigger('click').addClass('active');

            },
            complete: function (){

            }
        });
    });

    function tamanho_vars(){
        $('.tamanho_vars').click(function(e){
            e.preventDefault();
            $('.tamanho_vars').removeClass('active');
            $(this).addClass('active');
            var id_produto = $('input[name=id_produto]').val();
            var cor = $('.color.active').data('color');
            var tamanho = $(this).data('size');
            $.ajax({
                url: base_url+'LojaController/variationsPreco',
                method:'POST',
                data:{
                    'id_produto' : id_produto,
                    'tamanho' : tamanho,
                    'cor' : cor
                },
                dataType:'json',
                beforeSend: function(){
                    $('.append_valores').append('<progress></progress>');
                },
                success: function (data) {
                    $('.product-variation-price').show();
                    $('.append_valores').empty().append($("#valor_variacao").render(data));

                    $(".quantity").attr({
                        "max" : data.quantidade,
                        "min" : 1
                    });

                    $('.quantity-minus, .quantity-plus').click(function (e){
                        e.preventDefault();
                        var estoque_disponivel = $('.estoque_disponivel').val();
                        var selected_qty = $('.quantity').val();
                        if(selected_qty > estoque_disponivel ){
                            alert('Atenção a quantidade Disponivel é de: '+estoque_disponivel);
                            $('.quantity').val(estoque_disponivel);
                            return false;
                        }
                        return false;
                    });
                    $('.quantity').change(function (e){
                        e.preventDefault();
                        var estoque_disponivel = $('.estoque_disponivel').val();
                        var selected_qty = $('.quantity').val();
                        if(selected_qty > estoque_disponivel ){
                            alert('Atenção a quantidade Disponivel é de: '+estoque_disponivel);
                            $('.quantity').val(estoque_disponivel);
                            return false;
                        }
                        return false;
                    });
                },
                complete: function (){
                }
            });
        });
    }


    $('.variacao_cor').first().trigger('click').addClass('active');



});