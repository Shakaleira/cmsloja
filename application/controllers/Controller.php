<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usuarios');
        $this->load->model('Menus');
        $this->load->model('Logs');
        $this->load->model('Condicional');
        if(isset($this->session->userdata['logado']) == false){
          redirect(base_url());
        }
    }

    public function index()
	{
        $objCond = new Condicional();
        $objMenu = new Menus();
        $objLogs = new Logs();
        $data['scripts_js'] = array(
            'assets/js/demo7/pages/dashboard.js',
            'assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
            'assets/vendors/custom/gmaps/gmaps.js',
            'assets/js/widgets.js',
        );
        $data['scripts_css'] = array(
            'assets/vendors/custom/fullcalendar/fullcalendar.bundle.css'
        );
        $data['orcamento'] = $objCond->listaDashboard();
        $data['menus'] = $objMenu->select_all();
        $data['logs'] = $objLogs->select_all(6);
		$this->load->view('inc/inc_header',$data)->view('home')->view('inc/inc_footer',$data);
	}

    public function error_404()
    {
        $this->load->view('404');
    }


}
