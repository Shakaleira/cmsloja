<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class LojaController extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();

            $this->load->library('cart');
            $this->load->model('Usuarios')->model('Produto')->model('Config')->model('Orcamento')->model('Condicional')->model('Cliente');
            $this->load->model('Categoria');
            $this->load->model('Contato');

            $segmento = $this->uri->segment(2);

            if(
                !isset($_SESSION['upload']) or
                (isset($_SESSION['upload']) and $_SESSION['upload'] != $segmento)
            ){
                if($_SERVER['REQUEST_METHOD'] != 'POST'){
                    $objLogin = new Usuarios();
                    $objLogin->logarLoja($this->uri->segment(2));
                }else{
                    defined('PATH_STORE_UPLOAD') OR define('PATH_STORE_UPLOAD', UPLOAD_PATH.'/'.$_SESSION['upload'].'/');
                    defined('PATH_STORE') OR define('PATH_STORE', base_url('loja/'.$_SESSION['upload'].'/'));
                }
            }else{
                defined('PATH_STORE_UPLOAD') OR define('PATH_STORE_UPLOAD', UPLOAD_PATH.'/'.$_SESSION['upload'].'/');
                defined('PATH_STORE') OR define('PATH_STORE', base_url('loja/'.$_SESSION['upload'].'/'));
            }

        }

        public function index()
        {

            $objProd = new Produto();
            $objConf = new Config();
            $objCat = new Categoria();
            $dados['page'] = 'index';
            $dados['configuracoes'] = $objConf->getconf();
            $dados['produtos'] = $objProd->findBySite();
            $dados['promocoes_prod'] = $objProd->findBySitePromocao();
            $dados['categorias'] = $objCat->select_all();
            $this->load->view('loja/index',$dados);
        }

        public function quickview()
        {
            $objProd = new Produto();
            $dados = $objProd->findBySite($_POST['id']);
            $this->output->set_content_type('application/json')->set_output(json_encode($dados));
        }

        public function addcart()
        {
            $objProd = new Produto();
            $dados = $objProd->findBySite($_POST['id']);

            $data = array(
                'id'      => $dados->id,
                'qty'     => $_POST['quantidade'],
                'price'   => $dados->valor,
                'name'    => $dados->nome,
                'imagem'    => $dados->files,
                'options' => null
            );
            $this->cart->insert($data);

            $this->output->set_content_type('application/json')->set_output(json_encode($dados));
        }

        public function removeCart(){
            // Remove item from cart
            $this->cart->remove($_POST['id']);
            $this->output->set_content_type('application/json')->set_output(json_encode(['response' => true]));
        }

        public function sendEmail(){

            $this->load->library('email');

            $objConf = new Config();
            $objCond = new Condicional();
            $objProdC = new Produtoscondicional();
            $objClie = new Cliente();
            $objProd = new Produto();
            $dados['configuracoes'] = $objConf->getconf();

            // verifica existencia do cliente
            $find_email = $objClie->findOne(['email' => $_POST['email']]);
            if(empty($find_email)){
                $dados_cliente = [
                    'email' => $_POST['email'],
                    'nome' => $_POST['nome'],
                    'telefone' => $_POST['telefone'],
                ];
                $id_cliente = $objClie->insert($dados_cliente);
            }else{
                $id_cliente = $find_email->id;
            }

            // gero orçamento
            $transaction_code = rand(00000,99999);
            $dados_orcamento = [
                'cliente_id' => $id_cliente,
                'mensagem' => $_POST['mensagem'],
                'orcamento' => 1,
                'status_id' => 5,
                'transaction_id' => $transaction_code
            ];
            $objCond->insert($dados_orcamento);
            $insert_id = $this->db->insert_id();

            // insere orcamentos
            foreach ($this->cart->contents() as $key => $value) {
                $find_prod = $objProd->findOne(['id' => $value['id']]);

                $sobra = $find_prod->quantidade - 1;

                $objProd->update(['id' => $value['id'],'quantidade' => $sobra]);

                $dados_prod = [
                    'id_produto' => $value['id'],
                    'quantidade' => $value['qty'],
                    'id_condicional' => $insert_id,
                    'valor' => $value['price'],
                ];
                $objProdC->insert($dados_prod);
            }

            /* Recupera Produtos */
            $produtos = '';
            $produtos .= "<table width='100%'  cellspacing='0'>
                  <thead>
                     <tr>
                        <th>Nome</th>
                        <th>Foto</th>
                        <th>Quantidade</th>
                     </tr>
                  </thead>";
            foreach ($this->cart->contents() as $key => $value) {
                $produtos .= '
					<tbody>
					  <tr class="cart_item">
						 <td align="center" style="padding: 5px;border: 1px solid black;border-collapse: collapse;">'.  $value['name'].'</td>
						 <td align="center" style="padding: 5px;border: 1px solid black;border-collapse: collapse;">
			';
                if(!empty($value['files'])):
                    $produtos.= '<img style="width:52px;margin: 0;" src="'.$value['files'].'" alt="" title="">';
                else:
                    $produtos.=	'<img style="width:52px;margin: 0;" src="'.base_url().'images/nophoto.png" alt="" title="">';
                endif;
                $produtos.= '
						 </td>
						 <td align="center" style="padding: 5px;border: 1px solid black;border-collapse: collapse;">'.$value['qty'].'</td>
					  </tr>
				   </tbody>
				   <br>
				   <p>'.$_POST['mensagem'].'</p>
			';
            }

            $this->email->from('envios@meuestoqueonline.com.br', 'Solicitação de Pedidos');
            $this->email->to('envios@meuestoqueonline.com.br');
            $this->email->reply_to($_POST['email'], 'Cliente');
            $this->email->cc($_POST['email']);
            $this->email->subject('Orçamento Site');
            $this->email->message('<h3>Itens Selecionados</h3>'.$produtos);
            $this->cart->destroy();


            if($this->email->send()){
                $retorno = ['msg' => "Sua solicitação de orçamento foi enviada com sucesso! SE ESTE FOR O SEU PRIMEIRO CONTATO, FIQUE ATENTO(A) AO SEU LIXO ELETRONICO, POIS NOSSA RESPOSTA PODE SER DIRECIONADA PARA ESTA PASTA. Obrigado!<br><br> Voce esta sendo redirecionado em 5s!"];
                $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
            }else{
                print_r($this->email->print_debugger());
            }

        }

        public function sobre(){
            $objConf = new Config();
            $objCat = new Categoria();
            $dados['configuracoes'] = $objConf->getconf();
            $dados['categorias'] = $objCat->select_all();
            $this->load->view('loja/inc/header',$dados)->view('loja/sobre',$dados)->view('loja/inc/footer',$dados);
        }

        public function contato(){
            $objConf = new Config();
            $objCat = new Categoria();
            $dados['configuracoes'] = $objConf->getconf();
            $dados['categorias'] = $objCat->select_all();
            $this->load->view('loja/inc/header',$dados)->view('loja/contato')->view('loja/inc/footer',$dados);
        }

        public function sendcontato(){
            $objCont = new Contato();
            $this->load->library('email');

            $this->email->from('envios@meuestoqueonline.com.br', 'Contato');
            $this->email->to('envios@meuestoqueonline.com.br');
            $this->email->reply_to($_POST['email'], 'Cliente');
            $this->email->cc($_POST['email']);
            $this->email->subject('Contato Site');
            $this->email->message('Obrigado pelo seu contato, em breve retornaremos.');

            $objCont->insert($_POST);
            $retorno = ['msg' => true];
            $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
        }

        public function product_view(){
            $objConf = new Config();
            $objCat = new Categoria();
            $objPro = new Produto();
            $dados['configuracoes'] = $objConf->getconf();
            $dados['categorias'] = $objCat->select_all();
            $dados['produto'] = $objPro->findOneSite(['produtos.id'=> $this->uri->segment(5)]);
            $dados['produto_categoria'] = $objPro->findbyCategory($dados['produto'],3);
            $dados['produto_categoria_more'] = $objPro->findbyCategory($dados['produto'],3,false);
            $dados['produto_variacoes'] = $objPro->findVariationsSite($this->uri->segment(5));
            $dados['scripts_footer'] = [
              base_url().'assets/site/js/produtos.js'
            ];
            $this->load->view('loja/inc/header',$dados)->view('loja/produto')->view('loja/inc/footer',$dados);
        }

        public function category()
        {
            $objProd = new Produto();
            $objConf = new Config();
            $objCat = new Categoria();
            $dados['filtro_categoria'] = $this->uri->segment(4);
            $dados['filtro_nome'] = $this->uri->segment(6);
            $dados['configuracoes'] = $objConf->getconf();
            $dados['categorias'] = $objCat->select_all();
            $dados['categoria_info'] = $objCat->findOne(['id' => $this->uri->segment(4)]);
            $dados['produtos'] = $objProd->findBySiteSearch(['categoria' => $this->uri->segment(4),'term' => $this->uri->segment(6)]);
            $this->load->view('loja/inc/header',$dados)->view('loja/filtros',$dados)->view('loja/inc/footer',$dados);
        }

        public function variationsTamanho()
        {
            $this->load->model('ProdutosVariacao');
            $objProd = new ProdutosVariacao();
            $dados = $objProd->selectTamanhoVariacao($_POST);
            $this->output->set_content_type('application/json')->set_output(json_encode($dados));
        }

        public function variationsPreco()
        {
            $this->load->model('ProdutosVariacao');
            $objProd = new ProdutosVariacao();
            $dados = $objProd->selectPrecoVariacao($_POST);
            $this->output->set_content_type('application/json')->set_output(json_encode($dados));
        }
    }