<?php

    class Produtos extends CI_Controller
    {

        public function __construct()
        {
            parent::__construct();
            $this->load->model('Usuarios')->model('Produto')->model('Produtoscondicional')->model('Condicional');
            if(isset($this->session->userdata['logado']) == false){
                redirect(base_url());
            }
        }

        public function productByQtd(){
            $objProdutos = new Produto();
            $retorno = $objProdutos->findItens($_POST['data']);
            $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
        }

        public function productByConditional(){
            $objProdutosCondicional = new Produtoscondicional();
            $retorno = $objProdutosCondicional->findItensConditional($_POST['produto_id']);
            $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
        }

        public function deleteImage(){
            $objProdutos = new Produto();
            $retorno = $objProdutos->deleteImage($_POST);
            $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
        }

        public function atualizaItempProduto(){
            $objProdutos = new Produto();
            $objCondicional = new Produtoscondicional();
            // Atualiza estoque produto
            $retorno = $objProdutos->atualizaEstoque($_POST);
            // Atualiza Estoque Condicional
            $condicional = $objCondicional->removeItem($_POST);
            $this->output->set_content_type('application/json')->set_output(json_encode($retorno));
        }

    }