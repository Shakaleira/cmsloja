<?php
    /**
     * Created by PhpStorm.
     * User: diogo
     * Date: 12/10/2019
     * Time: 23:34
     */

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('Usuarios');
    }

    public function home()
    {
        if(isset($this->session->userdata['logado']) == true){
            redirect(base_url('home'));
        }
        $this->load->view('login');

    }

    public function autenticaUsuario(){
        $objUser = new Usuarios();
        $data= $objUser->logar($_POST);
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }


    /* Logout */
    public function logout(){
        $objUser = new Usuarios();
        $objUser->logout();
    }

    public function autenticaUsuarioLoja(){
        $objUser = new Usuarios();
        $data= $objUser->logarLoja($_POST);
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }

    public function trial()
    {
        if(isset($this->session->userdata['logado']) == true){
            redirect(base_url('home'));
        }
        $this->load->view('trial/login');
    }


    public function autenticaUsuarioTrial(){
        $objUser = new Usuarios();
        $data= $objUser->logar_trial($_POST);
        $this->output->set_content_type('application/json')->set_output(json_encode($data));
    }
}