<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    use Money\Currency;
    use Money\Money;
    include APPPATH . 'third_party/class.upload.php';



    /**
     * function set_combo
     *
     * @param varchar $item  modelo
     * @param integer $value valor referencia da tabela
     *
     * @return string
     */

    function set_combo($item,$valor_referencia)
    {
        $modelo = explode('_id',$item);
        $modelo = $modelo[0];

        $CI = get_instance();
        if(strpos($modelo, 'boleano') !== false){
            $modelo = 'Boleano';
        }
        $CI->load->model($modelo);

        $dados = $CI->{$modelo}->select_all();
        $special_fields = (isset($CI->{$modelo}->fields_special)) ? $CI->{$modelo}->fields_special : null;

        $multiple = null;
        if(!empty($special_fields) && !in_array($item,$special_fields)){
            $multiple = (isset($CI->{$modelo}->multiple) && $CI->{$modelo}->multiple == true) ? 'multiple' : null;
        }

        $combo = "<select class='select2 form-control form-control-solid form-control-lg  selectpicker_{$item} {$item}' name='{$item}' data-live-search='true' title='Selecione um item' {$multiple}>";
        $combo .='<option></option>';
        foreach ($dados as $value) {
            $selected = ($valor_referencia == $value->id) ? 'selected' : null;
            if($modelo == 'cor'){
                $combo .= "<option data-tokens='{$value->id}' value='{$value->id}' {$selected}>{$value->nome_referencia}</option>";
            }else{
                $combo .= "<option data-tokens='{$value->id}' value='{$value->id}' {$selected}>{$value->nome}</option>";
            }


        }
        $combo .= "</select>";

        return $combo;
    }

    function format_value($value,$type, $format = null){
        if(strpos($type,'dat') !== false) {
            if(empty($value)){
                $value = date('Y-m-d');
            }
            return (!empty($format)) ? date($format,strtotime($value)) : date('Y-m-d',strtotime($value));
        }elseif ($type == 'valor'){
            return  number_format($value,2,",",".");
        }
        return $value;
    }

    if ( ! function_exists('readFileConfig'))
    {
        function readFileConfig($param = null){

            $lines = file('conf.conf');
            $config = array();

            foreach ($lines as $l) {
                preg_match("/^(?P<key>.*)=(\s+)?(?P<value>.*)/", $l, $matches);
                if (isset($matches['key'])) {
                    $config[trim($matches['key'])] = trim($matches['value']);
                }
            }
            if(!empty($config) && !empty($param)){
                return $config[$param];
            }

            return $config;
        }
    }


    // filtra dados da requisição impondo regras negocio/filtros
    if ( ! function_exists('formFilter'))
    {
        function formFilter($data){
            foreach ($data as $ch => $valor){
                if($ch == 'valor'){
                    $data['valor'] = str_replace('R$','',$data['valor'] );
                    $data['valor'] = Valor($data['valor']);
                }
            }
            return $data;
        }

        function Valor($valor) {
            $verificaPonto = ".";
            if(strpos("[".$valor."]", "$verificaPonto")):
                $valor = str_replace('.','', $valor);
                $valor = str_replace(',','.', $valor);
            else:
                $valor = str_replace(',','.', $valor);
            endif;

            return $valor;
        }

    }


    if ( ! function_exists('gera_thumb'))
    {

        function gera_thumb($file, $w, $h, $crop=FALSE, $campo) {
            $arquivos = explode(',',$file);
            $return = "<div class='row'>";
            foreach ($arquivos  as $k => $v){
                $return .= "<div class='col-md-3 thumb_remove_{$campo}_{$k}'><center><div class='thumbnail'>";
                $return .= "<img src='".UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$v.'&w='.$w.'&h='.$h.'&zc=2' ."'>";
                $return .= "</div><a href='#' class='remove_thumb btn btn-danger btn-shadow-hover font-weight-bold mr-2' data-campo='{$campo}' data-position='{$k}'>Remover</a></center></div>";
            }
            $return .= "</div>";
            return $return;
        }

    }

    if ( ! function_exists('gera_thumb_default'))
    {

        function gera_thumb_default($file, $w, $h) {
            $arquivos = explode(',',$file);
            $return = "";
            foreach ($arquivos  as $k => $v){
                $return .= "<img src='".UPLOAD_PATH.$_SESSION['upload'].'/'.$v.'&w='.$w.'&h='.$h.'&zc=2' ."'>";
            }
            $return .= "";
            return $return;
        }

    }

    if ( ! function_exists('convertUrlFormat'))
    {
        function convertUrlFormat($string){
            return url_title(convert_accented_characters($string), '-',true);
        }
    }

    if ( ! function_exists('class_trait'))
    {
        function class_trait($key,$value,$modelo){
            if($modelo == 'cor' && $key == 'nome'){
                return 'spectrum_color';
            }
        }
    }
