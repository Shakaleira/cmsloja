<?php

class Produtoscondicional extends CI_Model
{
    /* --------------------------------------------------------------
         * VARIABLES
         * ------------------------------------------------------------ */

    /**
     * This model's default database table. Automatically
     * guessed by pluralising the model name.
     */
    private $table = 'produtos_condicional';

    /**
     * The database connection object. Will be set to the default
     * connection. This allows individual models to use different DBs
     * without overwriting CI's global $this->db connection.
     */
    public $_database;

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
    protected $primary_key = 'id';

    function __construct()
    {
        parent::__construct();
    }


    public $fields_restrict = ['id'];

    public function total()
    {
        $num_rows = $this->db
            ->from($this->table)
            ->count_all_results();
        return $num_rows;
    }

    public function select($fields = '*', $dados_where = null, $retorno = null, $wherein = null, $order = null, $qtd = null, $limit = null)
    {
        if (isset($_POST['sort'])) {
            $this->db->order_by($_POST['sort']['field'], $_POST['sort']['sort']);
        }
        $tempdb = clone $this->db;
        $num_rows = $tempdb
            ->from($this->table)
            ->count_all_results();
        if (isset($limit[1]) && !empty($limit[1])) {
            $this->db->limit($limit[1], $limit[0]);
        } else {
            $this->db->limit($limit[0]);
        }
        $this->db
            ->select($fields)
            ->from($this->table);
        $query = $this->db->get();
        $dados = $query->result();
        return ['dados' => $dados, 'qtd' => $num_rows];
    }

    public function get_list_table(){
        $fields = $this->db->list_fields($this->table);
        $data = [];
        foreach ($fields as $key => $val){
            $data[$val] = null;
        }
        return $data;
    }


    public function insert($dados)
    {
        $this->load->database('default');
        $dados = array_filter($dados);
        $query = $this->db->insert($this->table, $dados);
        return $query;
    }

    public function select_all(){
        $query = $this->db->get($this->table);
        $dados = $query->result();
        return $dados;
    }

    public function deleteAllWithParam($id){
        $this->load->database('default');
        $this->db->where('id_condicional', $id);
        $this->db->delete($this->table);
    }

    public function findOne($dados)
    {
        $query = $this->db->get_where($this->table, $dados);
        return $query->first_row();
    }

    public function findItensConditional($id){
        $this->load->database('default');
        $this->db->select("
                 produtos.id,
                 produtos.nome,
                 produtos_condicional.quantidade as quantidade,
                 produtos.quantidade as quantidade_produto, 
                 produtos.files,
                 cor.nome as cor,
                 tamanho.nome as tamanho,
                 condicional.status_id
                 ",FALSE)
            ->from($this->table)
            ->where('produtos_condicional.id_condicional',$id)
            ->join('produtos','produtos.id = produtos_condicional.id_produto','LEFT')
            ->join('condicional','condicional.id = produtos_condicional.id_condicional','LEFT')
            ->join('cor','cor.id = produtos.cor_id')
            ->join('tamanho','tamanho.id = produtos.tamanho_id');

        $data =  $this->db->get()->result();
        foreach ($data as $k => $v){
            $data[$k]->estoque = $v->quantidade_produto;
            if(!empty($v->files)){
                $img = explode(',',$v->files);
                $imagem = $img[0];
            }else{
                $imagem = null;
            }
            $data[$k]->imagem =  UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$imagem.'&w=80&h=80&zc=2';
        }

        return $data;

    }

    public function removeItem($dados){
        $this->load->database('default');
        $this->db->where('id_condicional', $dados['condicional'])->where('id_produto',$dados['produto_id']);
        $this->db->delete($this->table);
    }

}