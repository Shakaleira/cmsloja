<?php
    /**
     * Created by PhpStorm.
     * User: diogo
     * Date: 27/10/2019
     * Time: 02:57
     */

    class Niveis extends CI_Model
    {

        private $table = 'usuarios_nivel';

        function __construct()
        {
            parent::__construct();
        }

        public function insert($param = []){

            $data = $this->db->insert($this->table,$param);
            return $data;

        }

        public function select($dados_where = null,$retorno = null,$wherein = null,$order){
            if(!empty($order)){
                $query = $this->db->order_by($order[0],$order[1]);
            }
            if(empty($dados_where)){
                $query = $this->db->get($this->table);
            }else{
                //unset($dados_where['tabela']);
                $query = $this->db->get_where($this->table,$dados_where);
            }
            if(empty($retorno)):
                $dados = $query->result();
            else:
                $dados = $query->first_row();
            endif;
            return $dados;
        }
    }