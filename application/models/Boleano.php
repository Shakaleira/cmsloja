<?php


    class Boleano extends CI_Model
    {
        /* --------------------------------------------------------------
                 * VARIABLES
                 * ------------------------------------------------------------ */

        /**
         * This model's default database table. Automatically
         * guessed by pluralising the model name.
         */
        private $table = 'boleano';

        /**
         * The database connection object. Will be set to the default
         * connection. This allows individual models to use different DBs
         * without overwriting CI's global $this->db connection.
         */
        public $_database;

        /**
         * This model's default primary key or unique identifier.
         * Used by the get(), update() and delete() functions.
         */
        protected $primary_key = 'id';

        function __construct()
        {
            parent::__construct();
        }

        public $paginacao = 10;

        public $fields_datatable = [
            'id',
            'nome'
        ];

        public $fields_restrict = ['id','data_cadastro','ativo'];

        public function total()
        {
            $num_rows = $this->db
                ->from($this->table)
                ->count_all_results();
            return $num_rows;
        }

        public function select($fields = '*', $dados_where = null, $retorno = null, $wherein = null, $order = null, $qtd = null, $limit = null)
        {
            if (isset($_POST['sort'])) {
                $this->db->order_by($_POST['sort']['field'], $_POST['sort']['sort']);
            }
            $tempdb = clone $this->db;
            $num_rows = $tempdb
                ->from($this->table)
                ->count_all_results();
            if (isset($limit[1]) && !empty($limit[1])) {
                $this->db->limit($limit[1], $limit[0]);
            } else {
                $this->db->limit($limit[0]);
            }
            $this->db
                ->select($fields)
                ->from($this->table)->where('ativo', 1);
            if (!empty($dados_where)) {
                foreach ($this->fields_datatable as $k => $v){
                    $v = explode('as ',$v);
                    if($k  == 0){
                        $this->db->like($v[0],$dados_where);
                    }else{
                        $this->db->or_like($v[0], $dados_where);
                    }
                }
            }
            $query = $this->db->get();
            $dados = $query->result();
            return ['dados' => $dados, 'qtd' => $num_rows];
        }

        public function get_list_table(){
            $fields = $this->db->list_fields($this->table);
            $data = [];
            foreach ($fields as $key => $val){
                $data[$val] = null;
            }
            return $data;
        }


        public function insert($dados)
        {
            $dados = array_filter($dados);
            $query = $this->db->insert($this->table, $dados);
            return $query;
        }

        public function select_all(){
            $query = $this->db->get_where($this->table,['ativo' => 1]);
            $dados = $query->result();
            return $dados;
        }

        public function deletar($dado)
        {
            $this->db->where('id', $dado['id']);
            $query = $this->db->update($this->table, ['ativo' => 0 , 'data_inativo' => date('Y-m-d H:i:s')]);
            return $query;
        }

        public function findOne($dados)
        {
            $query = $this->db->get_where($this->table, $dados);
            return $query->first_row();
        }

        public function update($dado)
        {
            $this->db->where('id', $dado['id']);
            $query = $this->db->update($this->table, $dado);
            return $query;
        }

    }