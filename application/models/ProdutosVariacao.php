<?php

class ProdutosVariacao extends CI_Model
{
    /* --------------------------------------------------------------
         * VARIABLES
         * ------------------------------------------------------------ */

    /**
     * This model's default database table. Automatically
     * guessed by pluralising the model name.
     */
    private $table = 'produtos_variacao';

    /**
     * The database connection object. Will be set to the default
     * connection. This allows individual models to use different DBs
     * without overwriting CI's global $this->db connection.
     */
    public $_database;

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
    protected $primary_key = 'id';

    function __construct()
    {
        parent::__construct();
    }


    public $fields_restrict = ['id'];

    public function total()
    {
        $num_rows = $this->db
            ->from($this->table)
            ->count_all_results();
        return $num_rows;
    }

    public function select($fields = '*', $dados_where = null, $retorno = null, $wherein = null, $order = null, $qtd = null, $limit = null)
    {
        if (isset($_POST['sort'])) {
            $this->db->order_by($_POST['sort']['field'], $_POST['sort']['sort']);
        }
        $tempdb = clone $this->db;
        $num_rows = $tempdb
            ->from($this->table)
            ->count_all_results();
        if (isset($limit[1]) && !empty($limit[1])) {
            $this->db->limit($limit[1], $limit[0]);
        } else {
            $this->db->limit($limit[0]);
        }
        $this->db
            ->select($fields)
            ->from($this->table);
        $query = $this->db->get();
        $dados = $query->result();
        return ['dados' => $dados, 'qtd' => $num_rows];
    }

    public function get_list_table(){
        $fields = $this->db->list_fields($this->table);
        $data = [];
        foreach ($fields as $key => $val){
            $data[$val] = null;
        }
        return $data;
    }


    public function insert($dados)
    {
        $this->load->database('default');
        $dados = array_filter($dados);
        $query = $this->db->insert($this->table, $dados);
        return $query;
    }

    public function select_all(){
        $query = $this->db->get($this->table);
        $dados = $query->result();
        return $dados;
    }


    public function findOne($dados)
    {
        $query = $this->db->get_where($this->table, $dados);
        return $query->first_row();
    }

    public function deletar($dado)
    {
        $this->db->where('id', $dado);
        $query = $this->db->delete($this->table);
        return $query;
    }


    public function selectTamanhoVariacao($dados){
        $this->db->group_by('tamanho_id');
        $this->db->where('cor_id',$dados['color']);
        $this->db->where('produto_id',$dados['id_produto']);
        $this->db->join('tamanho as t','t.id = produtos_variacao.tamanho_id','left');
        $query = $this->db->get($this->table);
        $dados = $query->result();
        return $dados;
    }

    public function selectPrecoVariacao($dados){

        $this->db->where('cor_id',$dados['cor']);
        $this->db->where('tamanho_id',$dados['tamanho']);
        $this->db->where('produto_id',$dados['id_produto']);
        $this->db->join('tamanho as t','t.id = produtos_variacao.tamanho_id','left');
        $query = $this->db->get($this->table);
        $dados = $query->first_row();
        return $dados;
    }

}