<?php

/**
     * Created by PhpStorm.
     * User: diogo
     * Date: 21/10/2019
     * Time: 00:37
     */

class Logs extends CI_Model
{

    private $table = 'log';

    function __construct()
    {
        parent::__construct();
        $this->load->database('default');
    }

    public function insert($param = []){

      $data = $this->db->insert($this->table,$param);
      return $data;
    }

    public function select($dados_where = null,$retorno = null,$wherein = null,array $order){
        if(!empty($order)){
            $query = $this->db->order_by($order[0],$order[1]);
        }
        if(empty($dados_where)){
            $query = $this->db->get($this->table);
        }else{
            //unset($dados_where['tabela']);
            $query = $this->db->get_where($this->table,$dados_where);
        }
        if(empty($retorno)):
            $dados = $query->result();
        else:
            $dados = $query->first_row();
        endif;
        return $dados;
    }


    public function select_all($limit = 8){
        $this->db->limit($limit);
        $this->db->order_by('log.id','DESC');
        $this->db->join('log_acoes a','a.id = log.acao','left');
        $this->db->join('usuarios u','u.id = log.id_usuario','left');
        $this->db->select('a.nome as acao, u.nome as usuario, log.data_cadastro');
        $query = $this->db->get($this->table);
        $dados = $query->result();
        return $dados;
    }

}