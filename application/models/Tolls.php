<?php

class Tolls extends CI_Model {
	
    function __construct(){
        parent::__construct();
        $this->load->helper('text');
		set_time_limit(0);
    }


    public function formataCep($valor){
        return str_replace('-', '', $valor);
    }
	
	public function gera_senha(){
    	$characters = 'ABCDEFGHIJKLMNOPQRSTUVXZWY0123456789';
		$string = '';
		 for ($i = 0; $i < 5; $i++) {
		      $string .= $characters[rand(0, strlen($characters) - 1)];
		 }
		return $string;
    }
	
	/* Float Number */
    public function toFloat($numero){
        $numero = str_replace(',', '.', $numero);
        return floatval($numero);
    }
	
    public function arredondaValor($valor){
        return round($valor);
    }
	
	/* Converte URL  */
    public function convertUrlFormat($string){
        return url_title(convert_accented_characters($string), '-',true);
    }
	
	public function formataValor($valor){
    	$valor = str_replace('.','',$valor);
        $valor = str_replace(',','.',$valor);
        return $valor;
    }

    /* UPLOAD */
	function do_upload_produtos($nome_campo){

	    try{
            $this->load->helper(array('form', 'url'));

            if (!file_exists("./images/uploads/{$this->session->userdata('upload')}")) {
                mkdir("./images/uploads/{$this->session->userdata('upload')}", 0777, true);
            }

            $config['upload_path'] = "./images/uploads/{$this->session->userdata('upload')}";
            $config['allowed_types'] = 'jpeg|gif|jpg|png|pdf';
            $config['max_size'] = '5048';
            $config['overwrite']  = FALSE;//Não irá sobre-escrever o arquivo
            $config['encrypt_name'] = TRUE;//Trocará o nome do arquivo para um HASH

            //$field_name1 = "file";// Nome do campo INPUT do formulário
            $this->load->library('upload');
            //$this->upload->initialize($config);

            //Faz o upload
            $files = $_FILES;
            $cpt = count($_FILES['files']['name']);
            for($i=0; $i<$cpt; $i++)
            {
                $_FILES['files']['name']= $files['files']['name'][$i];
                $_FILES['files']['type']= $files['files']['type'][$i];
                $_FILES['files']['tmp_name']= $files['files']['tmp_name'][$i];
                $_FILES['files']['error']= $files['files']['error'][$i];
                $_FILES['files']['size']= $files['files']['size'][$i];

                $this->upload->initialize($config);
                // $this->upload->do_upload('files[]');
                if(!$this->upload->do_upload($nome_campo))
                {
                    return array('error' => 'Não foi possivel fazer upload','tipo' => $this->upload->display_errors());
                }else{
                    $upload_data = $this->upload->data();
                    $nome_do_arquivo_gravado = $upload_data['file_name'];

                    return $nome_do_arquivo_gravado;
                }
                sleep(10);
            }

        }catch(Exception $e){
	        return $e->getMessage();
        }


    }

    function do_upload($nome_campo){

        try{
            $this->load->helper(array('form', 'url'));

            if (!file_exists("./images/uploads/{$this->session->userdata('upload')}")) {
                mkdir("./images/uploads/{$this->session->userdata('upload')}", 0777, true);
            }

            $config['upload_path'] = "./images/uploads/{$this->session->userdata('upload')}";
            $config['allowed_types'] = '*';
            $config['max_size'] = '5048';
            $config['overwrite']  = FALSE;//Não irá sobre-escrever o arquivo
            $config['encrypt_name'] = TRUE;//Trocará o nome do arquivo para um HASH

            $this->load->library('upload');
            $this->upload->initialize($config);
            //Faz o upload
            if(!$this->upload->do_upload($nome_campo))
            {
                $error = array('error' => $this->upload->display_errors());
                return array('msg' => 'Não foi possivel fazer upload','error' => $error);
            }else{
                $upload_data = $this->upload->data();
                $nome_do_arquivo_gravado = $upload_data['file_name'];
                return array('msg' => $nome_do_arquivo_gravado);
            }
        }catch(Exception $e){
            return array('msg' => $e->getMessage());
        }

    }


    /* Modal */
	 public function modal($where = null,$tabela = null){
	   $prefixo = 'tb_finan_';
	   $where = array('id'=> $where);
	   $query = $this->db->get_where($prefixo.$tabela,$where);
	   $dados = $query->first_row();
	   return $dados;
    }
	
	/* Converte Resultados SERIALIZE */
	public function conexoes_serialize($base){		
	 $this->db->select('*')->from($base);
	 $query = $this->db->get();
	 return serialize($query->result_array());
	}
	
	/* Autocomplete */
	public function search($query){
        $this->db->select('*')->from('tb_finan_clientes')->like('nome', "$query", 'both')->limit(10);
        $query = $this->db->get();
        $produtos = $query->result();
        $array = array();
        $array['query'] = 'Unit';
        foreach ($produtos as $key => $value) {
            $array['suggestions'][] = array(
                'value' => "$value->nome", 
                'data' => "$value->id"
				);
        }
        return (object)$array;    
    }

    function My_Fixer($serialized_string) {
        // securities
        if (!preg_match('/^[aOs]:/', $serialized_string))
            return $serialized_string;
        if (@unserialize($serialized_string) !== false)
            return $serialized_string;

        return
            preg_replace_callback(
                '/s\:(\d+)\:\"(.*?)\";/s', function ($matches) {
                return 's:' . strlen($matches[2]) . ':"' . $matches[2] . '";';
            }, $serialized_string)
            ;
    }

    public function choices() {
        $this->db->select('valor,opcao')->from('tb_finan_options')->where_not_in('id', array(1, 2, 8, 14, 22));
        $query = $this->db->get();
        $data = $query->result();
        $array = array();
        foreach ($data as $key => $value) {
            $array['dado'][][$value->valor] = array(
                'data' => unserialize($this->My_Fixer($value->opcao))
            );
        }
        return $array;
    }
	
	########################################
	########################################
	########################################
	###	/* Tabela de Logs
		// 1- Ação realizada pelo usuario
		// 2- Tabela da Movimentação
		// 3- Id da tabela, para identificação posterior
	####	*/
	#########################################
	#########################################
	#########################################
	public function logs($acao,$tabela,$id_acao){
		$prefixo = 'tb_finan_';
		$data = array('id_acao' => $acao,'id_usuario' => $this->session->userdata('id'),'tabela'=>$tabela,'id_movimentacao' => $id_acao);
        $this->db->insert($prefixo.'logs',$data);
    }
	
	public function visualizados(){
		$prefixo = 'tb_finan_';
		$this->db->select('id_visualizado');
		$this->db->where_not_in('id_visualizado', $this->session->userdata('id')); 
		$this->db->from($prefixo.'logs');	
		$query = $this->db->get();
    	return $query->result();
    }
	
	/* Exibe Logs */
	public function show_logs(){
	 $dados = $this->logs_all();
	 $result = '<li class="vaza">'; 
	 foreach($dados as $value):
		$result .="
			<a href='javascript:;'>
				<span class='time'>agora</span>
				<span class='details'>
				<span class='label label-sm label-icon label-success'>
				<i class='fa fa-plus'></i>
				</span>".$value->usuario." </span>
			</a>
		";
	 endforeach;
	 $result .= '</li>'; 
	 return $result;
	}
	public function logs_full(){
		 $prefixo = 'tb_finan_';	
		 /* Verifica Nivel */
		 if($this->session->userdata('nivel') <> 'administrador'):
			// $acoes_disponiveis = '0';
			// $data = array('id_acao' => $acoes_disponiveis);
			// $this->db->where($data);
			$this->db->where('id_usuario',$this->session->userdata('id'));
		 endif;
		 $this->db->select('lo.*,tf.nome as usuario');
		 $this->db->from($prefixo.'logs lo');
		 $this->db->order_by('lo.id','DESC');
		 $this->db->join('tb_finan_usuarios tf','tf.id = lo.id_usuario','LEFT');
	     $query = $this->db->get();
         return $query->result(); 
	}
	
	public function logs_all(){
		 $prefixo = 'tb_finan_';	
		 /* Verifica Nivel */
		 if($this->session->userdata('nivel') <> 'administrador'):
			$acoes_disponiveis = '0';
			$data = array('id_acao' => $acoes_disponiveis);
			$this->db->where($data);
			$this->db->where('id_usuario',$this->session->userdata('id'));
		 endif;
		 $this->db->where_not_in('id_visualizado',$this->session->userdata('id')); 
		 $this->db->select('lo.*,tf.nome as usuario');
		 $this->db->from($prefixo.'logs lo');
		 $this->db->order_by('lo.id','DESC');
		 $this->db->join('tb_finan_usuarios tf','tf.id = lo.id_usuario','LEFT');
	     $query = $this->db->get();
         return $query->result(); 
	}
	
	public function conta_novos(){
		$prefixo = 'tb_finan_';
		$this->db->select("count(id) as contador");
		$this->db->from("tb_finan_logs");
		$this->db->where("id_visualizado IS NULL");
		$this->db->or_where("id_visualizado",'');
		$query = $this->db->get();
		$dados = $query->first_row();
		$data = $dados->contador;
		return $data;
	}
	########################################
					/* Chat */
	########################################
	
	public function chat_all($dados){
		$this->db->select('tc.*,tb_finan_usuarios.nome,tb_finan_usuarios.imagem ');
		$this->db->from('tb_finan_chat tc');
		$this->db->join('tb_finan_usuarios','tb_finan_usuarios.id = tc.id_remetente ','LEFT');
		$this->db->like('id_remetente', trim($this->session->userdata('id')));
		$this->db->like('id_destinatario', trim($dados['id_usuario']));
		$this->db->or_like('id_remetente', trim($dados['id_usuario'])); 
		$this->db->like('id_destinatario', trim($this->session->userdata('id'))); 
		$this->db->order_by('tc.id','ASC'); 		
		$query = $this->db->get();
		return $query->result(); 
	}
	
	
	public function chat_refresh($dados){
		$chat = $this->chat_all($dados);
		$dados ='';
		foreach($chat as $value):
			$dados .='
				<li class="
				'; 
					if($value->id_remetente == $this->session->userdata('id')){
						$dados .= 'out';
					}else{ 
						$dados .= 'in'; 
					}
			$dados .=' ">';
					 if(!empty($value->imagem)){
						$dados .= '<img class="avatar" alt="" src="'.THUMB.UPLOAD.$value->imagem.'&w=45&=h=45&zc=2"/>'; 
					 }else{
						$dados .= '<img class="avatar" alt="" src="'.THUMB.UPLOAD.'no-photo.jpg&w=45&=h=45&zc=2"/>';
					 } 
			$dados.='
					<div class="message">
						<span class="arrow"></span>
						<a href="#" class="name">'.$value->nome.' </a>
						<span class="datetime">as '.date('H:m:s',strtotime($value->data_cadastro)).' </span>
						<span class="body">'.$value->mensagem.' </span>
					</div>
				</li>
			'; 
		endforeach;
		
		echo $dados;
	}
	
	public function insert_chat($dados){
		if(!empty($dados['mensagem'])){
			$dados = array(
				'id_destinatario' => $dados['id_usuario'],
				'mensagem' => $dados['mensagem'],
				'id_remetente' => (INT)$this->session->userdata('id')
			);
			$this->db->insert("tb_finan_chat",$dados);
		}
		
	}
	
	########################################
	/* Calcula as diferenças entre horas */
	########################################
	
	public function Calculadiferenca($data){
		$data_inicial = date('Y-m-d');
		$data_final =  $data;
		$time_inicial = strtotime($data_inicial);
		$time_final = strtotime($data_final);
		$diferenca = $time_final - $time_inicial; 
		$dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
		if($dias > 0){
			echo "<strong>".$dias."</strong> dias para vencer";
		}elseif($dias == 0){
			echo'<a href="javascript:;" class="btn default yellow-stripe">
			Esta conta vence Hoje. </a>';
		}else{
			echo '<a href="javascript:;" class="btn default red-stripe">
			Esta conta esta Atrasada. </a>';
		}
	}
	
	public function CalculadiferencaBoleto($data){
		$data_inicial = date('Y-m-d');
		$data_final =  $data;
		$time_inicial = strtotime($data_inicial);
		$time_final = strtotime($data_final);
		$diferenca = $time_final - $time_inicial; 
		$dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
		if($dias > 0){
			return 0;
		}else{
			$resultado =  str_replace('-','',$dias);
			return (int)$resultado;
		}
	}

	
}
