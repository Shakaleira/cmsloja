<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Login/home';
$route['404_override'] = 'controller/error_404';
$route['translate_uri_dashes'] = FALSE;

// rotas
$route['login'] = 'Login/home';
$route['home'] = 'controller/index';
$route['cadastros/database_ybrida'] = 'cadastros/home';
$route['logout'] = 'Login/logout';
$route['usuarios'] = 'cadastros/registro';


$route['configuracoes/usuarios/:any'] = 'configuracoes/usuarios_edit';
$route['configuracoes/usuarios/:any/readonly'] = 'configuracoes/usuarios_edit';

$route['cadastros/:any'] = 'cadastros/registro';
$route['cadastros/processa_dados/:any'] = 'cadastros/processaDados';
$route['cadastros/edit/:any/:num'] = 'cadastros/dataControl';
$route['cadastros/new/:any'] = 'cadastros/dataControl';
$route['cadastros/:any/edit'] = 'cadastros/edit';
$route['cadastros/:any/insert'] = 'cadastros/insert';
$route['cadastros/:any/delete'] = 'cadastros/delete';

$route['returnColumns'] = 'cadastros/returnColumns';

###############################################################
############# Rotas da Loja ###################################
###############################################################


$route['loja/:any'] = 'LojaController/index';
$route['quickview'] = 'LojaController/quickview';
$route['addcart'] = 'LojaController/addcart';
$route['removecart'] = 'LojaController/removeCart';

$route['loja/:any/contato'] = 'LojaController/contato';
$route['loja/:any/sobre'] = 'LojaController/sobre';
$route['loja/:any/sobre'] = 'LojaController/sobre';
$route['contato_site'] = 'LojaController/sendcontato';
$route['loja/:any/produto/:any/:num'] = 'LojaController/product_view';
$route['loja/:any/categoria/:num/busca'] = 'LojaController/category';
$route['loja/:any/categoria/:num/busca/:any'] = 'LojaController/category';

// Trial
$route['trial'] = 'Login/trial';