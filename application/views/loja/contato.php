<main class="main">
    <!-- Start of Page Header -->
    <div class="page-header">
        <div class="container">
            <h1 class="page-title mb-0">Contato</h1>
        </div>
    </div>
    <!-- End of Page Header -->

    <!-- Start of Breadcrumb -->
    <nav class="breadcrumb-nav mb-10 pb-1">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="demo1.html">Home</a></li>
                <li>Contato</li>
            </ul>
        </div>
    </nav>
    <!-- End of Breadcrumb -->

    <!-- Start of PageContent -->
    <div class="page-content contact-us">
        <div class="container">
            <?php
                if($this->session->flashdata('message')):
            ?>
                <div class="alert alert-icon alert-success alert-bg alert-inline">
                    <h4 class="alert-title">
                    <i class="fas fa-check"></i>Muito Bem!</h4> Mensagem enviada com sucesso!
                </div>
            <?php endif;?>
            <section class="content-title-section mb-10">
                <h3 class="title title-center mb-3">Informações de Contato</h3>
                <p class="text-center">Escolha a melhor forma de entrar em contato:</p>
            </section>
            <!-- End of Contact Title Section -->

            <section class="contact-information-section mb-10">
                <div
                    class="owl-carousel owl-theme owl-loaded owl-drag"
                    data-owl-options="{
                        'items': 4,
                        'nav': false,
                        'dots': false,
                        'loop': false,
                        'margin': 20,
                        'responsive': {
                            '0': {
                                'items': 1
                            },
                            '480': {
                                'items': 2
                            },
                            '768': {
                                'items': 3
                            },
                            '992': {
                                'items': 4
                            }
                        }
                    }"
                >
                    <div class="owl-stage-outer">
                        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1260px;">
                            <div class="owl-item active" style="width: 295px; margin-right: 20px;">
                                <div class="icon-box text-center icon-box-primary">
                                    <span class="icon-box-icon icon-email">
                                        <i class="w-icon-envelop-closed"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">E-mail Address</h4>
                                        <p><?= $configuracoes->email?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 295px; margin-right: 20px;">
                                <div class="icon-box text-center icon-box-primary">
                                    <span class="icon-box-icon icon-headphone">
                                        <i class="w-icon-headphone"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Telefone</h4>
                                        <p><?= $configuracoes->telefone?></p>
                                    </div>
                                </div>
                            </div>
                            <div class="owl-item active" style="width: 295px; margin-right: 20px;">
                                <div class="icon-box text-center icon-box-primary">
                                    <span class="icon-box-icon icon-map-marker">
                                        <i class="w-icon-map-marker"></i>
                                    </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Endereço</h4>
                                        <p><?= $configuracoes->endereco?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button>
                    </div>
                    <div class="owl-dots disabled"></div>
                </div>
            </section>
            <!-- End of Contact Information section -->

            <hr class="divider mb-10 pb-1" />

            <section class="contact-section">
                <div class="row gutter-lg pb-3">
                    <div class="col-lg-12 mb-12">
                        <h4 class="title mb-3">Formulario de Mensagem</h4>
                        <form class="form formulario_contato">
                            <div class="form-group">
                                <label for="email_1">Assunto*</label>
                                <input type="text" id="assunto" name="assunto" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="username">Seu Nome*</label>
                                <input type="text" id="user" name="nome" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email_1">E-mail*</label>
                                <input type="email" id="email" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="email_1">Telefone</label>
                                <input type="text" id="tel" name="telefone" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="message">Mensagem*</label>
                                <textarea id="message" name="mensagem" cols="30" rows="5" class="form-control" required></textarea>
                            </div>
                            <button type="submit" class="btn btn-dark btn-rounded">Enviar Agora</button>
                        </form>
                        <center><div class="loader"></div></center>
                    </div>
                </div>
            </section>
            <!-- End of Contact Section -->
        </div>

    </div>
    <!-- End of PageContent -->
</main>
