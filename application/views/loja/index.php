<?php include('inc/header.php')?>
<main class="main">
    <div class="container pb-2">
        <?php include('inc/slide.php')?>
        <?php //include('inc/informacoes.php')?>
        <?php include('inc/banner_home.php')?>
        <?php include('inc/promocoes.php')?>
        <?php include('produtos_categoria/index.php')?>
        <?php include('inc/marcas.php')?>
    </div>
    <!-- End of Container -->
</main>
<?php include('inc/clientes.php')?>
<?php include('inc/footer.php')?>
