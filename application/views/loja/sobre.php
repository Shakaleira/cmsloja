<main class="main">
    <!-- Start of Page Header -->
    <div class="page-header">
        <div class="container">
            <h1 class="page-title mb-0">A Empresa</h1>
        </div>
    </div>
    <!-- End of Page Header -->

    <!-- Start of Breadcrumb -->
    <nav class="breadcrumb-nav mb-10 pb-8">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="<?= base_url()?>">Home</a></li>
                <li>A Empresa</li>
            </ul>
        </div>
    </nav>
    <!-- End of Breadcrumb -->

    <!-- Start of Page Content -->
    <div class="page-content">
        <div class="container">
            <section class="introduce mb-10 pb-10">
                <h2 class="title title-center">
                    <?= (isset($configuracoes->sobre_titulo) && !empty($configuracoes->sobre_titulo)) ? $configuracoes->sobre_titulo : 'Titulo' ?>
                </h2>
                <p class="mx-auto text-center">
                    <?= (isset($configuracoes->sobre_subtitulo) && !empty($configuracoes->sobre_subtitulo)) ? $configuracoes->sobre_subtitulo : 'Subtitulo' ?>
                </p>
            </section>
        </div>

        <section class="boost-section pt-10 pb-10">
            <div class="container mt-10 mb-9">
                <div class="row align-items-center mb-10">
                    <div class="col-md-6 mb-8">
                        <figure class="br-lg">
                            <img src="<?= (isset($configuracoes->input_principal_sobre) && !empty($configuracoes->input_principal_sobre)) ? UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$configuracoes->input_principal_sobre.'&w=610&h=450' : '' ?>" alt="Banner" style="background-color: #9e9da2;" />
                        </figure>
                    </div>
                    <div class="col-md-6 pl-lg-8 mb-8">
                        <h4 class="title text-left"><?= (isset($configuracoes->sobre_titulo_descricao) && !empty($configuracoes->sobre_titulo_descricao)) ? $configuracoes->sobre_titulo_descricao : 'Titulo Descrição das atividades da empresa' ?></h4>
                        <p class="mb-6">
                            <?= (isset($configuracoes->sobre_descricao) && !empty($configuracoes->sobre_descricao)) ? $configuracoes->sobre_descricao : 'Descrição das atividades da empresa' ?>
                        </p>
                    </div>
                </div>
                <?php
                    $imagens_empresa = (!empty($configuracoes->imagens_empresa)) ? explode(',',$configuracoes->imagens_empresa) : null ;
                    if(!empty($imagens_empresa)):
                ?>
                <div class="awards-wrapper">
                    <h4 class="title title-center font-weight-bold mb-10 pb-1 ls-25">Fotos</h4>
                    <div
                        class="owl-carousel owl-theme owl-loaded owl-drag"
                        data-owl-options="{
                                'nav': false,
                                'dots': true,
                                'margin': 20,
                            }"
                    >
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1260px;">
                                <?php
                                    foreach($imagens_empresa as $ko => $imgs):
                                ?>
                                    <div class="owl-item active" style="width: 295px; margin-right: 20px;">
                                        <div class="image-box-wrapper">
                                            <div class="image-box text-center">
                                                <figure>
                                                    <img src="<?= UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$imgs.'&w=109&h=105&q=100' ?>" alt="Award Image" />
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="owl-nav disabled">
                            <button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button>
                        </div>
                        <div class="owl-dots disabled">
                            <button role="presentation" class="owl-dot active"><span></span></button>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </section>


    </div>
</main>
