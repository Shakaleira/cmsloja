<div class="dropdown-box" style="overflow-x:scroll;">
    <div class="cart-header">
        <span>Meu Carrinho</span>
        <a href="#" class="btn-close">Fechar<i class="w-icon-long-arrow-right"></i></a>
    </div>
    <div class="conteudo_carrinho">
        <div class="products">
            <?php
                if(count($this->cart->contents()) <= 0):
            ?>
                <p>Nenhum Item Selecionado</p>
            <?php else:?>
                <?php foreach ($this->cart->contents() as $items): ?>
                    <div class="product product-cart">
                        <div class="product-detail">
                            <a href="#" class="product-name"><?= $items['name'] ?></a>
                            <div class="price-box">
                                <span class="product-quantity"><?= $items['qty']?></span>
                                <?php
                                    if(!empty($items['price']) && $items['price'] != 0):
                                ?>
                                     <span class="product-price"><?= number_format($items['price'],2,',','.')?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <figure class="product-media">
                            <a href="product-default.html">
                                <img src="<?= $items['imagem']?>" alt="product" height="84" width="94">
                            </a>
                        </figure>
                        <button data-id='<?= $items['rowid']?>' class="remove_item btn btn-link btn-close">
                            <i class="fas fa-times" style="margin-top: -15px;margin-left: -3px;"></i>
                        </button>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <?php
            if(count($this->cart->contents()) > 0):
        ?>
            <div class="cart-total">
                <label>Subtotal:</label>
                <span class="price">R$ <?= $this->cart->format_number($this->cart->total()); ?></span>
            </div>

            <div class="cart-action">
                <a href="#" class="btn btn-primary col-md-12 btn-rounded finalizar_carrinho">Finalizar</a>
            </div>
            <hr>
        <?php endif; ?>
    </div>
    <div class="hidden_form" style="display:none;">
        <p>Voce esta prestes a enviar um orçamento com os itens listados no seu carrinho.</p>
        <form class="form contact-us-form orcamento_carrinho" action="#" method="post">
            <div class="form-group">
                <label for="username">Seu Nome</label>
                <input type="text" id="username" name="nome" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email_1">Seu E-mail</label>
                <input type="email" id="email_1" name="email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email_1">Seu Telefone</label>
                <input type="telefone" id="telefone" name="telefone" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="message">Mensagem</label>
                <textarea id="message" name="mensagem" cols="30" rows="5" class="form-control"></textarea>
            </div>
            <button type="submit" class="btn btn-dark btn-rounded">Enviar</button>
            <button type="button" class="btn btn-dark btn-rounded voltar">Voltar</button>
        </form>
    </div>
    <br clear="all">
    <div class="loader col-md-12"></div>
</div>