<?php
    if(!empty($promocoes_prod)):
?>
<div class="title-link-wrapper mb-3">
    <h2 class="title mb-0 pt-2 pb-2">Promoções</h2>
    <a href="shop-boxed-banner.html" class="mb-0">Todos os Itens<i class="w-icon-long-arrow-right"></i></a>
</div>
<div class="owl-carousel owl-theme product-wrapper owl-loaded owl-drag" data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '576': {
                            'items': 3
                        },
                        '768': {
                            'items': 4
                        },
                        '992': {
                            'items': 5
                        },
                        '1200': {
                            'items': 6
                        }
                    }
                }">
    <div class="owl-stage-outer">
        <div class="owl-stage" >
            <?php
                foreach ($promocoes_prod as $k => $vo):
                $imagem = (!empty($vo->files)) ? explode(',',$vo->files) : null ;
                $destaque = (isset($imagem[0])) ? $imagem[0] : null ;
            ?>
            <div class="owl-item active">
                <div class="product product-simple text-center">
                    <figure class="product-media">
                        <a href="product-default.html">
                            <img src="<?= (!empty($destaque)) ? UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$destaque.'&w=260&h=291' : APPPATH.'/images/nophoto.png'?>" alt="Product" >
                        </a>
                        <div class="product-action-vertical">
                            <a href="#" class="btn-product-icon btn-wishlist w-icon-heart" title="Add to wishlist"></a>
                            <a href="#" class="btn-product-icon btn-compare w-icon-compare" title="Add to Compare"></a>
                        </div>
                        <div class="product-action">
                            <a href="#" class="btn-product btn-quickview" title="Quick View">Quick View</a>
                        </div>
                    </figure>
                    <div class="product-details">
                        <h4 class="product-name"><a href="product-default.html"><?= $vo->nome?></a></h4>
                        <div class="product-pa-wrapper">
                            <div class="product-price">
                                <?php
                                    if(!empty($vo->valor) && $vo->valor != 0):
                                ?>
                                    <ins class="new-price">R$ <?= number_format($vo->valor,2,',','.')?></ins>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="sold-by">
                            Cor: <a href="#"><?= $vo->cor?></a>
                        </div>
                    </div>
                </div>
            </div>
            <?php
                endforeach;
            ?>
        </div>
    </div>
    <div class="owl-nav disabled">
        <button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button>
    </div>
    <div class="owl-dots disabled">
        <button role="presentation" class="owl-dot active"><span></span></button>
    </div>
</div>
<?php endif ?>