<?php
    if(isset($configuracoes->clientes)):
        ?>
        <div class="title-link-wrapper mb-4 appear-animate">
            <h2 class="title mb-0 ls-normal appear-animate pb-1">Clientes Atendidos</h2>
        </div>
        <div class="owl-carousel owl-theme owl-shadow-carousel appear-animate row cols-xl-8 cols-lg-6 cols-md-4 cols-2 mb-10 pb-2"
             data-owl-options="{
                'nav': false,
                'dots': true,
                'margin': 20,
                'responsive': {
                    '0': {
                        'items': 2
                    },
                    '576': {
                        'items': 3
                    },
                    '768': {
                        'items': 5
                    },
                    '992': {
                        'items': 6
                    },
                    '1200': {
                        'items': 8,
                        'dots': false
                    }
                }
            }">
            <?php
                for($i=1;$i < 8;$i++):
                    ?>
                    <div class="product-wrap mb-0">
                        <div class="product text-center product-absolute">
                            <figure class="product-media">
                                <a href="product-defaproduct-default.html">
                                    <img src="https://w7.pngwing.com/pngs/793/681/png-transparent-the-coca-cola-company-fizzy-drinks-beverages-mangolia-food-text-logo.png" alt="Category image"
                                         width="213" height="238" style="background-color: #fff" />
                                </a>
                            </figure>
                            <h4 class="product-name">
                                <a href="product-default.html">The Coca Logo</a>
                            </h4>
                        </div>
                    </div>
                <?php endfor;?>
            <!-- End of Product Wrap -->
        </div>
    <?php
    endif;
?>