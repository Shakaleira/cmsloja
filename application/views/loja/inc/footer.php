<footer class="footer" >
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-4 col-sm-6">
                    <div class="widget widget-about">
                        <a href="<?= base_url()?>" class="logo-footer">
                            <img src="<?= base_url('images/mini.png')?>" alt="logo" />
                        </a>
                        <div class="widget-body">
                            <p class="widget-about-title">Duvidas? Entre em contato :</p>
                            <a href="tel:18005707777" class="widget-about-call"><?= $configuracoes->telefone?></a>
                            <p class="widget-about-desc">
                                <?= $configuracoes->endereco?>
                            </p>

                            <div class="social-icons social-icons-colored">
                                <a href="<?= $configuracoes->facebook?>" style="<?= (empty($configuracoes->facebook)) ? 'display:none' : '' ?>" class="social-icon social-facebook w-icon-facebook"></a>
                                <a href="<?= $configuracoes->instagram?>" style="<?= (empty($configuracoes->instagram)) ? 'display:none' : '' ?>" class="social-icon social-instagram w-icon-instagram"></a>
                                <a href="<?= $configuracoes->youtube?>" style="<?= (empty($configuracoes->youtube)) ? 'display:none' : '' ?>" class="social-icon social-youtube w-icon-youtube"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h3 class="widget-title">A Empresa</h3>
                        <ul class="widget-body">
                            <li><a href="#">Quem Somos</a></li>
                            <li><a href="#">Time</a></li>
                            <li><a href="#">Vagas de Emprego</a></li>
                            <li><a href="#">Contatos</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">Meus Dados</h4>
                        <ul class="widget-body">

                            <li><a href="#">login</a></li>
                            <li><a href="#">Ajuda</a></li>
                            <li><a href="#">Meus Orçamentos</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="widget">
                        <h4 class="widget-title">Serviços Ao Consumidor</h4>
                        <ul class="widget-body">
                            <li><a href="#">Metodos de Pagamento</a></li>
                            <li><a href="#">Politica de Troca</a></li>
                            <li><a href="#">Atendimento</a></li>
                            <li><a href="#">Envio</a></li>
                            <li><a href="#">Termos e Condições</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of Page-wrapper -->
        <!-- Start of Footer -->
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="footer-left">
                <p class="copyright">Copyright © <?= date('Y')?> Dfolio Sistemas. All Rights Reserved.</p>
            </div>
            <div class="footer-right">
                <span class="payment-label mr-lg-8">Ambiente seguro</span>
                <figure class="logo">
                    <img src="<?= base_url('images/mini.png')?>" alt="logo" />
                </figure>
            </div>
        </div>
    </div>
</footer>
<!-- End of Footer -->
<!-- Start of Scroll Top -->
<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
<!-- End of Scroll Top -->
<div id='render_produto' style="display:none;" class="render_produto"></div>
<script id="quickview" type="text/x-jsrender">
    <div class=" product product-single ">
        <div class="row gutter-lg">
            <div class="col-md-6 mb-4 mb-md-0">
                <div class="product-gallery product-gallery-sticky mb-0">
                    <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1 gutter-no">
                        <figure style='height:400px;overflow:hidden;' class="product-image">
                            <img src="{{:files}}"  data-zoom-image="{{:files}}" alt="Water Boil Black Utensil" width="800" height="900">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="col-md-6 overflow-hidden p-relative">
                <div class="product-details scrollable pl-0">
                    <h2 class="product-title">{{:nome}}</h2>
                    <div class="product-bm-wrapper">
                        <div class="product-meta">
                            <div class="product-categories">
                                Categoria:
                                <span class="product-category"><a href="#">{{:categoria}}</a></span>
                            </div>
                        </div>
                    </div>

                    {{if valor != 0 }}
                        <div class="product-price">R$ {{:valor}}</div>
                    {{/if}}
                    <hr class="product-divider">

                    <div class="product-short-desc">
                        <ul class="list-type-check list-style-none">
                            <li>{{:descricao}}</li>
                        </ul>
                    </div>

                    <hr class="product-divider">

                    <div class='col-md-12'>
                        <div class="product-qty-form">
                            <div class="input-group">
                                <label>Quantidade:</label>
                                <input name='quantidade' id='quantidade_itens' class="form-control" data-max='{{:quantidade}}' value='1' placeholder="1" type="number" min="1" max="{{:quantidade}}">
                            </div>
                        </div>
                    </div>
                    <div class="product-form col-md-12">
                        <button data-id='{{:id}}' class="btn btn-primary btn-cart">
                            <i class="w-icon-cart"></i>
                            <span>Adicionar ao Carrinho</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script>
    var ambiente;
    ambiente = '<?= $actual_link = "http://$_SERVER[HTTP_HOST]";?>';
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#22b9ff",
                "light": "#ffffff",
                "dark": "#282a3c",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
            }
        }
    };
</script>
<!--end:: Global Mandatory Vendors -->
<script src="<?= base_url()?>assets/site/js/jquery.min.js"></script>
<script src="<?= base_url()?>assets/js/global.js" type="text/javascript"></script>
<!--<script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>-->
<script src="<?= base_url()?>assets/site/js/parallax.min.js"></script>
<script src="<?= base_url()?>assets/site/js/sticky.min.js"></script>
<script src="<?= base_url()?>assets/site/js/jquery.plugin.min.js"></script>
<script src="<?= base_url()?>assets/site/js/owl.carousel.min.js"></script>
<script src="<?= base_url()?>assets/site/js/imagesloaded.pkgd.min.js"></script>
<script src="<?= base_url()?>assets/site/js/skrollr.min.js"></script>
<script src="<?= base_url()?>assets/site/js/jquery.magnific-popup.min.js"></script>
<script src="<?= base_url()?>assets/site/js/jquery.zoom.min.js"></script>
<script src="<?= base_url()?>assets/site/js/photoswipe.min.js"></script>
<script src="<?= base_url()?>assets/site/js/photoswipe-ui-default.min"></script>
<script src="<?= base_url()?>assets/site/js/jquery.countdown.min.js"></script>
<script src="<?= base_url()?>assets/site/js/main.min.js"></script>
<script src="<?= base_url()?>assets/js/jsrender.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/eModal.min.js" type="text/javascript"></script>
<script>
    $('input[type=number][max]:not([max=""])').on('input', function(ev) {
        var $this = $(this);
        var maxlength = $this.attr('max').length;
        var value = $this.val();
        if (value && value.length >= maxlength) {
            $this.val(value.substr(0, maxlength));
        }
    });
</script>
<script src="<?= base_url()?>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/site/js/scripts_site.js"></script>
<?php
    if(isset($scripts_footer)):
          foreach ($scripts_footer as $k => $v):
?>
        <script src="<?= $v ?>"></script>
    <?php  endforeach; ?>
<?php  endif; ?>
</body>
</html>