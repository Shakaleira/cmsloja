<div class="owl-carousel owl-theme icon-box-wrapper appear-animate br-sm mt-6 mb-10 owl-loaded owl-drag fadeIn appear-animation-visible" data-owl-options="{
   'nav': false,
   'dots': false,
   'loop': true,
   'autoplay': true,
   'autoplayTimeout': 4000,
   'responsive': {
   '0': {
   'items': 1
   },
   '576': {
   'items': 2
   },
   '768': {
   'items': 3
   },
   '992': {
   'items': 3
   },
   '1200': {
   'items': 4
   }
   }
   }" style="animation-duration: 1.2s;">
    <div class="owl-stage-outer">
        <div class="owl-stage" style="transform: translate3d(-1301px, 0px, 0px); transition: all 1s ease 0s; width: 3903px;">
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-shipping">
               <i class="w-icon-truck"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Free Shipping &amp; Returns</h4>
                        <p class="text-default">For all orders over $99</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-payment">
               <i class="w-icon-bag"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Secure Payment</h4>
                        <p class="text-default">We ensure secure payment</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-money">
               <span class="icon-box-icon icon-money">
               <i class="w-icon-money"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Money Back Guarantee</h4>
                        <p class="text-default">Any back within 30 days</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-chat">
               <span class="icon-box-icon icon-chat">
               <i class="w-icon-chat"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Customer Support</h4>
                        <p class="text-default">Call or email us 24/7</p>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-shipping">
               <i class="w-icon-truck"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Free Shipping &amp; Returns</h4>
                        <p class="text-default">For all orders over $99</p>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-payment">
               <i class="w-icon-bag"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Secure Payment</h4>
                        <p class="text-default">We ensure secure payment</p>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-money">
               <span class="icon-box-icon icon-money">
               <i class="w-icon-money"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Money Back Guarantee</h4>
                        <p class="text-default">Any back within 30 days</p>
                    </div>
                </div>
            </div>
            <div class="owl-item active" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-chat">
               <span class="icon-box-icon icon-chat">
               <i class="w-icon-chat"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Customer Support</h4>
                        <p class="text-default">Call or email us 24/7</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-shipping">
               <i class="w-icon-truck"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Free Shipping &amp; Returns</h4>
                        <p class="text-default">For all orders over $99</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark">
               <span class="icon-box-icon icon-payment">
               <i class="w-icon-bag"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Secure Payment</h4>
                        <p class="text-default">We ensure secure payment</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-money">
               <span class="icon-box-icon icon-money">
               <i class="w-icon-money"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Money Back Guarantee</h4>
                        <p class="text-default">Any back within 30 days</p>
                    </div>
                </div>
            </div>
            <div class="owl-item cloned" style="width: 325.25px;">
                <div class="icon-box icon-box-side text-dark icon-box-chat">
               <span class="icon-box-icon icon-chat">
               <i class="w-icon-chat"></i>
               </span>
                    <div class="icon-box-content">
                        <h4 class="icon-box-title">Customer Support</h4>
                        <p class="text-default">Call or email us 24/7</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button><button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button></div>
    <div class="owl-dots disabled"></div>
</div>