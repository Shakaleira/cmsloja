<?php
    if(isset($configuracoes->marcas)):
?>
    <!-- End of Owl Carousel -->
    <h2 class="title text-left mb-5 appear-animate">Nossas Marcas</h2>
    <div class="owl-carousel owl-theme row cols-xl-8 cols-lg-6 cols-md-4 cols-sm-3 cols-2 brands-wrapper br-sm mb-9 appear-animate"
         data-owl-options="{
            'nav': false,
            'dots': false,
            'autoplay': true,
            'autoplayTimeout': 4000,
            'loop': true,
            'margin': 20,
        }">
        <?php
            for($i=1;$i < 8;$i++):
        ?>
            <figure>
                <img src="https://w7.pngwing.com/pngs/595/571/png-transparent-swoosh-nike-logo-just-do-it-adidas-nike-black-desktop-wallpaper-symbol.png" alt="Brand" width="310" height="180" />
            </figure>
        <?php endfor;?>
    </div>
<?php endif;?>