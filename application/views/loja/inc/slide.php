<div class="intro-wrapper mt-4">
    <div class="owl-carousel owl-theme owl-dot-inner animation-slider owl-loaded owl-drag" data-owl-options="{
                        'nav': false,
                        'dots': true
                    }">
        <div class="owl-stage-outer">
            <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 3009px;">
                <?php
                    if(empty($slides)):
                ?>
                    <div class="owl-item active" style="width: 1003px;">
                        <div class="banner banner-fixed intro-slide intro-slide1 br-sm" style="background-image: url(<?= UPLOAD_PATH_WITHOUT_THUMB.'slide_nophoto.png'?>); background-color: #E8EAEF;" data-index="1">
                        </div>
                    </div>
                <?php endif;?>
                <?php
                    $slides = (isset($configuracoes->slides)) ? $configuracoes->slides : null;
                    if(!empty($slides)):
                    $slides = explode(',',$slides);
                       foreach ($slides as $key => $value):
                ?>
                    <div class="owl-item active" style="width: 1003px;">
                        <div class="banner banner-fixed intro-slide intro-slide1 br-sm" style="background-image: url(<?= UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$value.'&w=1003&h=509'?>); background-color: #E8EAEF;" data-index="1">
                            <div class="banner-content y-50 text-right">
                                <!--<div class="slide-animate fadeInUpShorter show-content" data-animation-options="{
                                    'name': 'fadeInUpShorter', 'duration': '1s'
                                }" style="animation-duration: 1s; animation-delay: 0.2s;">
                                    <h5 class="banner-subtitle text-uppercase font-weight-bold mb-2">Deals And
                                        Promotions</h5>
                                    <h3 class="banner-title text-capitalize ls-25">
                                        <span class="text-primary">Introducing</span><br>
                                        Fashion Lifestyle<br>Collection
                                    </h3> <a href="demo8-shop.html" class="btn btn-dark btn-outline btn-rounded btn-icon-right">
                                        Shop Now<i class="w-icon-long-arrow-right"></i>
                                    </a> </div>-->
                            </div>
                        </div>
                    </div>
                   <?php endforeach; ?>
               <?php endif; ?>
            </div>
        </div>
        <?php
            if(!empty($slides)):
        ?>
        <div class="owl-nav disabled">
            <button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button>
            <button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button>
        </div>
        <div class="owl-dots">
            <button role="presentation" class="owl-dot active"><span></span></button>
            <button role="presentation" class="owl-dot"><span></span></button>
            <button role="presentation" class="owl-dot"><span></span></button>
        </div>
        <?php endif; ?>
    </div>
    <!-- End of Owl Carousel -->
</div>