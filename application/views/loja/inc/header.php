
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <title>Meu Estoque Online - Minha Loja</title>

    <meta name="keywords" content="estoque online, controle seu estoque, estoque, online, recursos, orcamento, controle" />
    <meta name="description" content="Meu Estoque Online - Bootstrap eCommerce Template">
    <meta name="author" content="DFolio Sistemas">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="assets/images/icons/favicon.png">

    <!-- WebFont.js -->
    <script>
        var link = '<?= base_url()?>';
        WebFontConfig = {
            google: { families: ['Poppins:400,500,600,700,800',] }
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = link+'assets/site/js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>

    <link rel="preload" href="<?= base_url()?>assets/site/fonts/fa-regular-400.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?= base_url()?>assets/site/fonts/fa-solid-900.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?= base_url()?>assets/site/fonts/fa-brands-400.woff2" as="font" type="font/woff2"
          crossorigin="anonymous">
    <link rel="preload" href="<?= base_url()?>assets/site/fonts/wolmart.ttf?png09e" as="font" type="font/ttf" crossorigin="anonymous">

    <!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/all.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Plugins CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/photoswipe.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/default-skin.min.css">

    <link href="<?= base_url()?>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />

    <!-- Default CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/demo.css">

    <link rel="stylesheet" type="text/css" href="<?= base_url()?>assets/site/css/style.min.css">

    <!-- Latest compiled and minified CSS -->
    <!--<link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css" >-->
    <style>
        <?php if(isset($configuracoes->color_header) && !empty($configuracoes->color_header)): ?>
            .header-middle{
                background:<?= $configuracoes->color_header?>!important
            }
        <?php endif; ?>

        <?php if(isset($configuracoes->color_header_top) && !empty($configuracoes->color_header_top)): ?>
            .header-top{
                background:<?= $configuracoes->color_header_top?>!important
            }
        <?php endif; ?>
    </style>
</head>
<body class="home">
<div class="page-wrapper">
    <header class="header">
        <div class="header-top">
            <div class="container">
                <div class="header-left">
                    <p class="welcome-msg">Bem Vindo a Minha Loja Virtual!</p>
                </div>
                <div class="header-right">
                    <!-- End of Dropdown Menu -->
                    <span class="divider d-lg-show"></span>
                    <a href="#" class="d-lg-show "><i class="w-icon-account"></i>Login</a>
                    <span class="delimiter d-lg-show">/</span>
                    <a href="#" class="ml-0 d-lg-show ">Registro</a>
                </div>
            </div>
        </div>
        <div class="header-middle">
            <div class="container">
                <div class="header-left mr-md-4">
                    <a href="#" class="mobile-menu-toggle text-white w-icon-hamburger">
                    </a>
                    <a href="<?= base_url('loja/'.$_SESSION['upload'])?>" class="logo ml-lg-0">
                        <img src="<?= (empty($configuracoes->logo)) ? base_url('images/white-mini.png') : UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$configuracoes->logo ?>&w=100&ct=1" alt="logo" >
                    </a>
                    <form method="post" action="#" class="busca_geral input-wrapper header-search hs-expanded hs-round d-none d-md-flex">
                        <div class="select-box bg-white">
                            <select id="category" name="category" required>
                                <option value="">Categorias</option>
                                <?php
                                    foreach ($categorias as $k => $v):
                                ?>
                                    <option value="<?= $v->id ?>" <?= (isset($filtro_categoria) && !empty($filtro_categoria) && $v->id == $filtro_categoria) ? 'selected' : '' ?>><?= strtoupper($v->nome)?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <input type="text" class="form-control bg-white" value='<?= (isset($filtro_nome) && !empty($filtro_nome)) ? $filtro_nome : '' ?>' name="search" id="search" placeholder="Buscar em...">
                        <button class="btn btn-search" type="submit"><i class="w-icon-search"></i>
                        </button>
                        <input type="hidden" name="base_path" value="<?= PATH_STORE ?>">
                    </form>
                </div>
                <div class="header-right ml-4">
                    <div class="header-call d-xs-show d-lg-flex align-items-center">
                        <a href="tel:#" class="w-icon-call text-white"></a>
                        <div class="call-info d-lg-show">
                            <h4 class="chat font-weight-normal font-size-md text-normal ls-normal text-white mb-0">
                                <a href="mailto:#" class="text-capitalize text-white">Contato</a> :</h4>
                            <a href="tel:#" class="phone-number font-weight-bolder text-white ls-50"><?= $configuracoes->telefone?></a>
                        </div>
                    </div>
                    <div class="dropdown cart-dropdown cart-offcanvas mr-0 mr-lg-2">
                        <div class="cart-overlay"></div>
                        <a href="#" class="cart-toggle label-down link text-white">
                            <i class="w-icon-cart">
                                <span style="background:<?= (isset($configuracoes->cor_primaria) && !empty($configuracoes->cor_primaria)) ? $configuracoes->cor_primaria : '#336699' ?>" class="cart-count"><?= $this->cart->total_items() ?></span>
                            </i>
                            <span class="cart-label">Carrinho</span>
                        </a>
                        <?php include('carrinho.php')?>
                        <!-- End of Dropdown Box -->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <header class="header header-border">
        <?php include('menu.php')?>
    </header>
