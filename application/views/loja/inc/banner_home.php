<div class="owl-carousel owl-theme category-banner-3cols pt-2 pb-10 owl-loaded owl-drag" data-owl-options="{
   'nav': false,
   'dots': true,
   'margin': 20,
   }">
    <div class="owl-stage-outer">
        <div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0.7s ease 0s; width: 1323px;">
            <?php
                $banner = (isset($configuracoes->banner)) ? $configuracoes->banner : null;
                if(!empty($banner)):
                    $banner = explode(',',$banner);
                    $banner = array_slice($banner,0,3);
                    foreach ($banner as $key => $value):
            ?>
                    <div class="owl-item active" style="width: 421px; margin-right: 20px;">
                        <div class="banner banner-fixed category-banner br-sm">
                            <figure>
                                <img src="<?= UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$value.'&w=447&h=230'?>" alt="Category Banner" width="447" height="230" style="background-color: #cfd1cf;">
                            </figure>
                            <!--<div class="banner-content y-50">
                                <h3 class="banner-title text-capitalize ls-25 mb-0">For Men's</h3>
                                <div class="banner-price-info text-uppercase text-default ls-25 font-weight-bold">Starting
                                    at <span class="text-secondary">$29.00</span>
                                </div>
                                <hr class="banner-divider bg-dark">
                                <a href="demo8-shop.html" class="btn btn-dark btn-link btn-outline btn-icon-right btn-slide-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>-->
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="owl-nav disabled">
        <button type="button" role="presentation" class="owl-prev"><i class="w-icon-angle-left"></i></button>
        <button type="button" role="presentation" class="owl-next"><i class="w-icon-angle-right"></i></button>
    </div>
    <div class="owl-dots disabled">
        <button role="presentation" class="owl-dot active"><span></span></button>
    </div>
</div>