<div class="sticky-content-wrapper" style="">
    <div class="header-bottom sticky-content fix-top sticky-header has-dropdown" style="">
        <div class="container">
            <div class="inner-wrap">
                <div class="header-left">
                    <div id='menu_suspenso' class="dropdown category-dropdown has-border <?= (isset($page) && $page == 'index') ? 'show-dropdown' :  '' ?>" data-visible="true">
                        <a href="#" class="category-toggle text-dark bg-white text-capitalize" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-display="static" title="Categorias">
                            <i class="w-icon-category"></i>
                            <span>Categorias</span>
                        </a>
                        <div class="dropdown-box text-default">
                            <ul class="menu vertical-menu category-menu">
                                <?php
                                    foreach ($categorias as $k => $v):
                                ?>
                                <li>
                                    <a href="#">
                                        <i class="<?= (!empty($v->icone)) ? $v->icone : 'w-icon-gift'?>"></i><?= strtoupper($v->nome)?>
                                    </a>
                                </li>
                                <?php endforeach;?>
                                <li>
                                    <a href="#" class="font-weight-bold text-uppercase ls-25">
                                        Ver Todos<i class="w-icon-angle-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <nav class="main-nav">
                        <ul class="menu">
                            <li class="active">
                                <a href="<?= PATH_STORE ?>?>">Home</a>
                            </li>
                            <li>
                                <a href="<?= PATH_STORE ?>/sobre">Sobre</a>
                            </li>
                            <li>
                                <a href="<?= PATH_STORE ?>/contato">contato</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="header-right">
                    <!--<a href="#" class="d-xl-show"><i class="w-icon-map-marker mr-1"></i>Track Order</a>
                    <a href="#"><i class="w-icon-sale"></i>Daily Deals</a>-->
                </div>
            </div>
        </div>
    </div>
</div>