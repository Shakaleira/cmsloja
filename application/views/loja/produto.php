<main class="main mb-10 pb-1">
    <!-- Start of Breadcrumb -->
    <nav class="breadcrumb-nav container">
        <ul class="breadcrumb bb-no">
            <li><a href="demo1.html">Home</a></li>
            <li>Produto</li>
            <li><?= strtoupper($produto->nome)?></li>
        </ul>
        <ul class="product-nav list-style-none">
            <li class="product-nav-prev">
                <a href="#">
                    <i class="w-icon-angle-left"></i>
                </a>
                <span class="product-nav-popup">
                    <img src="assets/images/products/product-nav-prev.jpg" alt="Product" width="110"
                         height="110" />
                    <span class="product-name">Soft Sound Maker</span>
                </span>
            </li>
            <li class="product-nav-next">
                <a href="#">
                    <i class="w-icon-angle-right"></i>
                </a>
                <span class="product-nav-popup">
                            <img src="assets/images/products/product-nav-next.jpg" alt="Product" width="110"
                                 height="110" />
                            <span class="product-name">Fabulous Sound Speaker</span>
                        </span>
            </li>
        </ul>
    </nav>
    <!-- End of Breadcrumb -->

    <!-- Start of Page Content -->
    <div class="page-content">
        <div class="container">
            <div class="row gutter-lg">
                <div class="main-content">
                    <div class="product product-single row">
                        <div class="col-md-6 mb-6">
                            <div class="product-gallery product-gallery-sticky">
                                <div class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1 gutter-no">
                                    <?php
                                        $imagens = (isset($produto->files) && !empty($produto->files) ) ? explode(',',$produto->files) : '';
                                        foreach($imagens as $k => $v):
                                    ?>
                                    <figure class="product-image">
                                        <img src="<?= PATH_STORE_UPLOAD.$v.'&w=800&h=900'?>"
                                             data-zoom-image="<?= PATH_STORE_UPLOAD.$v.'&w=800&h=900'?>"
                                             alt="Electronics Black Wrist Watch" width="800" height="900">
                                    </figure>
                                    <?php endforeach; ?>
                                </div>
                                <div class="product-thumbs-wrap">
                                    <div class="product-thumbs row cols-4 gutter-sm">
                                        <?php
                                            $imagens = (isset($produto->files) && !empty($produto->files) ) ? explode(',',$produto->files) : '';
                                            foreach($imagens as $k => $v):
                                        ?>
                                        <div class="product-thumb active">
                                            <img src="<?= PATH_STORE_UPLOAD.$v.'&w=800&h=900'?>"
                                                 alt="Product Thumb" width="800" height="900">
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                    <button class="thumb-up disabled"><i class="w-icon-angle-left"></i></button>
                                    <button class="thumb-down disabled"><i
                                                class="w-icon-angle-right"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4 mb-md-6">
                            <div class="product-details" data-sticky-options="{'minWidth': 767}">
                                <h2 class="product-title"><?= strtoupper($produto->nome)?></h2>
                                <div class="product-bm-wrapper">
                                    <?php
                                        if(isset($produto->marca)):
                                    ?>
                                        <figure class="brand">
                                            <img src="assets/images/products/brand/brand-1.jpg" alt="Brand"
                                                 width="102" height="48" />
                                        </figure>
                                    <?php endif; ?>
                                    <div class="product-meta">
                                        <div class="product-categories">
                                            Categoria:
                                            <span class="product-category"><a href="#"><?= $produto->categoria?></a></span>
                                        </div>
                                        <div class="product-sku">
                                            SKU: <span><?= $produto->id?></span>
                                        </div>
                                    </div>
                                </div>
                                <hr class="product-divider">
                                <!--<div class="product-price"><ins class="new-price">R$ <?= number_format($produto->valor,2,",","."); ?></ins></div>
                                <div class="ratings-container">
                                    <div class="ratings-full">
                                        <span class="ratings" style="width: 80%;"></span>
                                        <span class="tooltiptext tooltip-top"></span>
                                    </div>
                                    <a href="#product-tab-reviews" class="rating-reviews scroll-to">(3
                                        Reviews)</a>
                                </div>

                                <div class="product-short-desc">
                                    <ul class="list-type-check list-style-none">
                                        <li>Ultrices eros in cursus turpis massa cursus mattis.</li>
                                        <li>Volutpat ac tincidunt vitae semper quis lectus.</li>
                                        <li>Aliquam id diam maecenas ultricies mi eget mauris.</li>
                                    </ul>
                                </div>

                                <hr class="product-divider">-->

                                <div class="product-form product-variation-form product-color-swatch">
                                    <label>Cor:</label>
                                    <div class="d-flex align-items-center product-variations" id="cores_variacoes">
                                        <?php
                                            foreach ($produto_variacoes as $k => $v):
                                        ?>
                                            <a href="#" class="color variacao_cor" data-color='<?= $v->cor_id ?>' style="background-color: <?= $v->cor ?>"></a>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="product-form product-variation-form product-size-swatch">
                                    <div class="append_tamanhos"></div>
                                </div>

                                <div class="product-variation-price">
                                    <div class="append_valores"></div>
                                </div>

                                <div class="fix-bottom product-sticky-content sticky-content">
                                    <div class="product-form container">
                                        <div class="product-qty-form">
                                            <div class="input-group">
                                                <input class="quantity form-control" type="number" min="1">
                                                <button class="quantity-plus w-icon-plus"></button>
                                                <button class="quantity-minus w-icon-minus"></button>
                                            </div>
                                        </div>
                                        <button class="btn btn-primary btn-cart">
                                            <i class="w-icon-cart"></i>
                                            <span>+ Carrinho</span>
                                        </button>
                                    </div>
                                </div>

                                <!--redes sociais-->
                            </div>
                        </div>
                    </div>
                    <!--<div class="frequently-bought-together mt-5">
                        <h2 class="title title-underline">Frequently Bought Together</h2>
                        <div class="bought-together-products row mt-8 pb-4">
                            <div class="product product-wrap text-center">
                                <figure class="product-media">
                                    <img src="assets/images/products/default/bought-1.jpg" alt="Product"
                                         width="138" height="138" />
                                    <div class="product-checkbox">
                                        <input type="checkbox" class="custom-checkbox" id="product_check1"
                                               name="product_check1">
                                        <label></label>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name">
                                        <a href="#">Electronics Black Wrist Watch</a>
                                    </h4>
                                    <div class="product-price">$40.00</div>
                                </div>
                            </div>
                            <div class="product product-wrap text-center">
                                <figure class="product-media">
                                    <img src="assets/images/products/default/bought-2.jpg" alt="Product"
                                         width="138" height="138" />
                                    <div class="product-checkbox">
                                        <input type="checkbox" class="custom-checkbox" id="product_check2"
                                               name="product_check2">
                                        <label></label>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name">
                                        <a href="#">Apple Laptop</a>
                                    </h4>
                                    <div class="product-price">$1,800.00</div>
                                </div>
                            </div>
                            <div class="product product-wrap text-center">
                                <figure class="product-media">
                                    <img src="assets/images/products/default/bought-3.jpg" alt="Product"
                                         width="138" height="138" />
                                    <div class="product-checkbox">
                                        <input type="checkbox" class="custom-checkbox" id="product_check3"
                                               name="product_check3">
                                        <label></label>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name">
                                        <a href="#">White Lenovo Headphone</a>
                                    </h4>
                                    <div class="product-price">$34.00</div>
                                </div>
                            </div>
                            <div class="product-button">
                                <div class="bought-price font-weight-bolder text-primary ls-50">$1,874.00</div>
                                <div class="bought-count">For 3 items</div>
                                <a href="cart.html" class="btn btn-dark btn-rounded">Add All To Cart</a>
                            </div>
                        </div>
                    </div>-->
                    <div class="tab tab-nav-boxed tab-nav-underline product-tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#product-tab-description" class="nav-link active">Descrição</a>
                            </li>
                            <li class="nav-item">
                                <a href="#product-tab-specification" class="nav-link">Especificação</a>
                            </li>
                            <li class="nav-item">
                                <a href="#product-tab-reviews" class="nav-link">Opiniões dos Clientes (0)</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="product-tab-description">
                                <div class="row mb-4">
                                    <div class="col-md-6 mb-5">
                                        <h4 class="title tab-pane-title font-weight-bold mb-2">Detalhes</h4>
                                        <p class="mb-4"><?= $produto->descricao?></p>
                                    </div>
                                    <!--<div class="col-md-6 mb-5">
                                        <div class="banner banner-video product-video br-xs">
                                            <figure class="banner-media">
                                                <a href="#">
                                                    <img src="assets/images/products/video-banner-610x300.jpg"
                                                         alt="banner" width="610" height="300"
                                                         style="background-color: #bebebe;">
                                                </a>
                                                <a class="btn-play-video btn-iframe" href="assets/video/memory-of-a-woman.mp4"></a>
                                            </figure>
                                        </div>
                                    </div>-->
                                </div>
                                <!--<div class="row cols-md-3">
                                    <div class="mb-3">
                                        <h5 class="sub-title font-weight-bold"><span class="mr-3">1.</span>Free
                                            Shipping &amp; Return</h5>
                                        <p class="detail pl-5">We offer free shipping for products on orders
                                            above 50$ and offer free delivery for all orders in US.</p>
                                    </div>
                                    <div class="mb-3">
                                        <h5 class="sub-title font-weight-bold"><span>2.</span>Free and Easy
                                            Returns</h5>
                                        <p class="detail pl-5">We guarantee our products and you could get back
                                            all of your money anytime you want in 30 days.</p>
                                    </div>
                                    <div class="mb-3">
                                        <h5 class="sub-title font-weight-bold"><span>3.</span>Special Financing
                                        </h5>
                                        <p class="detail pl-5">Get 20%-50% off items over 50$ for a month or
                                            over 250$ for a year with our special credit card.</p>
                                    </div>
                                </div>-->
                            </div>
                            <div class="tab-pane" id="product-tab-specification">
                                <ul class="list-none">
                                    <li>
                                        <label>Model</label>
                                        <p>Skysuite 320</p>
                                    </li>
                                    <li>
                                        <label>Color</label>
                                        <p>Black</p>
                                    </li>
                                    <li>
                                        <label>Size</label>
                                        <p>Large, Small</p>
                                    </li>
                                    <li>
                                        <label>Guarantee Time</label>
                                        <p>3 Months</p>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane" id="product-tab-reviews">
                                <div class="row mb-4">
                                    <div class="col-xl-4 col-lg-5 mb-4">
                                        <div class="ratings-wrapper">
                                            <div class="avg-rating-container">
                                                <h4 class="avg-mark font-weight-bolder ls-50">3.3</h4>
                                                <div class="avg-rating">
                                                    <p class="text-dark mb-1">Average Rating</p>
                                                    <div class="ratings-container">
                                                        <div class="ratings-full">
                                                            <span class="ratings" style="width: 60%;"></span>
                                                            <span class="tooltiptext tooltip-top"></span>
                                                        </div>
                                                        <a href="#" class="rating-reviews">(3 Reviews)</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div
                                                    class="ratings-value d-flex align-items-center text-dark ls-25">
                                                        <span
                                                                class="text-dark font-weight-bold">66.7%</span>Recommended<span
                                                        class="count">(2 of 3)</span>
                                            </div>
                                            <div class="ratings-list">
                                                <div class="ratings-container">
                                                    <div class="ratings-full">
                                                        <span class="ratings" style="width: 100%;"></span>
                                                        <span class="tooltiptext tooltip-top"></span>
                                                    </div>
                                                    <div class="progress-bar progress-bar-sm ">
                                                        <span></span>
                                                    </div>
                                                    <div class="progress-value">
                                                        <mark>70%</mark>
                                                    </div>
                                                </div>
                                                <div class="ratings-container">
                                                    <div class="ratings-full">
                                                        <span class="ratings" style="width: 80%;"></span>
                                                        <span class="tooltiptext tooltip-top"></span>
                                                    </div>
                                                    <div class="progress-bar progress-bar-sm ">
                                                        <span></span>
                                                    </div>
                                                    <div class="progress-value">
                                                        <mark>30%</mark>
                                                    </div>
                                                </div>
                                                <div class="ratings-container">
                                                    <div class="ratings-full">
                                                        <span class="ratings" style="width: 60%;"></span>
                                                        <span class="tooltiptext tooltip-top"></span>
                                                    </div>
                                                    <div class="progress-bar progress-bar-sm ">
                                                        <span></span>
                                                    </div>
                                                    <div class="progress-value">
                                                        <mark>40%</mark>
                                                    </div>
                                                </div>
                                                <div class="ratings-container">
                                                    <div class="ratings-full">
                                                        <span class="ratings" style="width: 40%;"></span>
                                                        <span class="tooltiptext tooltip-top"></span>
                                                    </div>
                                                    <div class="progress-bar progress-bar-sm ">
                                                        <span></span>
                                                    </div>
                                                    <div class="progress-value">
                                                        <mark>0%</mark>
                                                    </div>
                                                </div>
                                                <div class="ratings-container">
                                                    <div class="ratings-full">
                                                        <span class="ratings" style="width: 20%;"></span>
                                                        <span class="tooltiptext tooltip-top"></span>
                                                    </div>
                                                    <div class="progress-bar progress-bar-sm ">
                                                        <span></span>
                                                    </div>
                                                    <div class="progress-value">
                                                        <mark>0%</mark>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-8 col-lg-7 mb-4">
                                        <div class="review-form-wrapper">
                                            <h3 class="title tab-pane-title font-weight-bold mb-1">Submit Your
                                                Review</h3>
                                            <p class="mb-3">Your email address will not be published. Required
                                                fields are marked *</p>
                                            <form action="#" method="POST" class="review-form">
                                                <div class="rating-form">
                                                    <label for="rating">Your Rating Of This Product :</label>
                                                    <span class="rating-stars">
                                                                <a class="star-1" href="#">1</a>
                                                                <a class="star-2" href="#">2</a>
                                                                <a class="star-3" href="#">3</a>
                                                                <a class="star-4" href="#">4</a>
                                                                <a class="star-5" href="#">5</a>
                                                            </span>
                                                    <select name="rating" id="rating" required=""
                                                            style="display: none;">
                                                        <option value="">Rate…</option>
                                                        <option value="5">Perfect</option>
                                                        <option value="4">Good</option>
                                                        <option value="3">Average</option>
                                                        <option value="2">Not that bad</option>
                                                        <option value="1">Very poor</option>
                                                    </select>
                                                </div>
                                                <textarea cols="30" rows="6"
                                                          placeholder="Write Your Review Here..." class="form-control"
                                                          id="review"></textarea>
                                                <div class="row gutter-md">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control"
                                                               placeholder="Your Name" id="author">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control"
                                                               placeholder="Your Email" id="email_1">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" class="custom-checkbox"
                                                           id="save-checkbox">
                                                    <label for="save-checkbox">Save my name, email, and website
                                                        in this browser for the next time I comment.</label>
                                                </div>
                                                <button type="submit" class="btn btn-dark">Submit
                                                    Review</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab tab-nav-boxed tab-nav-outline tab-nav-center">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a href="#show-all" class="nav-link active">Show All</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#helpful-positive" class="nav-link">Most Helpful
                                                Positive</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#helpful-negative" class="nav-link">Most Helpful
                                                Negative</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#highest-rating" class="nav-link">Highest Rating</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#lowest-rating" class="nav-link">Lowest Rating</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="show-all">
                                            <ul class="comments list-style-none">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/1-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:54 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 60%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>pellentesque habitant morbi tristique senectus
                                                                et. In dictum non consectetur a erat.
                                                                Nunc ultrices eros in cursus turpis massa
                                                                tincidunt ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-1.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-1-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/2-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:52 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 80%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>Nullam a magna porttitor, dictum risus nec,
                                                                faucibus sapien.
                                                                Ultrices eros in cursus turpis massa tincidunt
                                                                ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-2.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-2.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-3.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-3.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/3-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:21 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 60%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>In fermentum et sollicitudin ac orci phasellus. A
                                                                condimentum vitae
                                                                sapien pellentesque habitant morbi tristique
                                                                senectus et. In dictum
                                                                non consectetur a erat. Nunc scelerisque viverra
                                                                mauris in aliquam sem fringilla.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (0)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (1)
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="helpful-positive">
                                            <ul class="comments list-style-none">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/1-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:54 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 60%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>pellentesque habitant morbi tristique senectus
                                                                et. In dictum non consectetur a erat.
                                                                Nunc ultrices eros in cursus turpis massa
                                                                tincidunt ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-1.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-1.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/2-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:52 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 80%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>Nullam a magna porttitor, dictum risus nec,
                                                                faucibus sapien.
                                                                Ultrices eros in cursus turpis massa tincidunt
                                                                ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-2.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-2-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-3.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-3-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="helpful-negative">
                                            <ul class="comments list-style-none">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/3-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:21 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 60%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>In fermentum et sollicitudin ac orci phasellus. A
                                                                condimentum vitae
                                                                sapien pellentesque habitant morbi tristique
                                                                senectus et. In dictum
                                                                non consectetur a erat. Nunc scelerisque viverra
                                                                mauris in aliquam sem fringilla.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (0)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (1)
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="highest-rating">
                                            <ul class="comments list-style-none">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/2-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:52 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 80%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>Nullam a magna porttitor, dictum risus nec,
                                                                faucibus sapien.
                                                                Ultrices eros in cursus turpis massa tincidunt
                                                                ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-2.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-2-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-3.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-3-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-pane" id="lowest-rating">
                                            <ul class="comments list-style-none">
                                                <li class="comment">
                                                    <div class="comment-body">
                                                        <figure class="comment-avatar">
                                                            <img src="assets/images/agents/1-100x100.png"
                                                                 alt="Commenter Avatar" width="90" height="90">
                                                        </figure>
                                                        <div class="comment-content">
                                                            <h4 class="comment-author">
                                                                <a href="#">John Doe</a>
                                                                <span class="comment-date">March 22, 2021 at
                                                                            1:54 pm</span>
                                                            </h4>
                                                            <div class="ratings-container comment-rating">
                                                                <div class="ratings-full">
                                                                            <span class="ratings"
                                                                                  style="width: 60%;"></span>
                                                                    <span
                                                                            class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <p>pellentesque habitant morbi tristique senectus
                                                                et. In dictum non consectetur a erat.
                                                                Nunc ultrices eros in cursus turpis massa
                                                                tincidunt ante in nibh mauris cursus mattis.
                                                                Cras ornare arcu dui vivamus arcu felis bibendum
                                                                ut tristique.</p>
                                                            <div class="comment-action">
                                                                <a href="#"
                                                                   class="btn btn-secondary btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-up"></i>Helpful (1)
                                                                </a>
                                                                <a href="#"
                                                                   class="btn btn-dark btn-link btn-underline sm btn-icon-left font-weight-normal text-capitalize">
                                                                    <i class="far fa-thumbs-down"></i>Unhelpful
                                                                    (0)
                                                                </a>
                                                                <div class="review-image">
                                                                    <a href="#">
                                                                        <figure>
                                                                            <img src="assets/images/products/default/review-img-3.jpg"
                                                                                 width="60" height="60"
                                                                                 alt="Attachment image of John Doe's review on Electronics Black Wrist Watch"
                                                                                 data-zoom-image="assets/images/products/default/review-img-3-800x900.jpg" />
                                                                        </figure>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section class="vendor-product-section">
                        <div class="title-link-wrapper mb-4">
                            <h4 class="title text-left">Mais Produtos dessa Categoria</h4>
                            <a href="#" class="btn btn-dark btn-link btn-slide-right btn-icon-right">Todos
                                Produtos<i class="w-icon-long-arrow-right"></i></a>
                        </div>
                        <div class="owl-carousel owl-theme row cols-lg-3 cols-md-4 cols-sm-3 cols-2"
                             data-owl-options="{
                                    'nav': false,
                                    'dots': false,
                                    'margin': 20,
                                    'responsive': {
                                        '0': {
                                            'items': 2
                                        },
                                        '576': {
                                            'items': 3
                                        },
                                        '768': {
                                            'items': 4
                                        },
                                        '992': {
                                            'items': 3
                                        }
                                    }
                                }">

                            <?php
                                foreach($produto_categoria as $ko => $vo):
                                $imagem = (!empty($vo->files)) ? explode(',',$vo->files) : null ;
                                $destaque = (isset($imagem[0])) ? $imagem[0] : null ;
                            ?>
                            <div class="product">
                                <figure class="product-media">
                                    <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>">
                                        <img src="<?= (!empty($destaque)) ? PATH_STORE_UPLOAD.$destaque.'&w=300&h=338' : APPPATH.'/images/nophoto.png'?>" alt="Product"
                                             width="300" height="338" />
                                    </a>
                                    <div class="product-action">
                                        <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>" class="btn-product" title="Visualizar">Visualizar</a>
                                    </div>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat"><a href="shop-banner-sidebar.html"><?= $produto->categoria?></a>
                                    </div>
                                    <h4 class="product-name"><a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>"><?= $produto->nome?></a>
                                    </h4>
                                    <!--<div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="product-default.html" class="rating-reviews">(3 reviews)</a>
                                    </div>-->
                                    <div class="product-pa-wrapper">
                                        <div class="product-price">R$ <?= number_format($produto->valor,2,",","."); ?></div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </section>
                </div>
                <!-- End of Main Content -->
                <aside class="sidebar product-sidebar sidebar-fixed right-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay"></div>
                    <a class="sidebar-close" href="#"><i class="close-icon"></i></a>
                    <a href="#" class="sidebar-toggle d-flex d-lg-none"><i class="fas fa-chevron-left"></i></a>
                    <div class="sidebar-content scrollable">
                        <div class="sticky-sidebar">
                            <!--<div class="widget widget-icon-box mb-6">
                                <div class="icon-box icon-box-side">
                                            <span class="icon-box-icon text-dark">
                                                <i class="w-icon-truck"></i>
                                            </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Free Shipping & Returns</h4>
                                        <p>For all orders over $99</p>
                                    </div>
                                </div>
                                <div class="icon-box icon-box-side">
                                            <span class="icon-box-icon text-dark">
                                                <i class="w-icon-bag"></i>
                                            </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Secure Payment</h4>
                                        <p>We ensure secure payment</p>
                                    </div>
                                </div>
                                <div class="icon-box icon-box-side">
                                            <span class="icon-box-icon text-dark">
                                                <i class="w-icon-money"></i>
                                            </span>
                                    <div class="icon-box-content">
                                        <h4 class="icon-box-title">Money Back Guarantee</h4>
                                        <p>Any back within 30 days</p>
                                    </div>
                                </div>
                            </div>-->
                            <!-- End of Widget Icon Box -->

                            <!--<div class="widget widget-banner mb-9">
                                <div class="banner banner-fixed br-sm">
                                    <figure>
                                        <img src="assets/images/shop/banner3.jpg" alt="Banner" width="266"
                                             height="220" style="background-color: #1D2D44;" />
                                    </figure>
                                    <div class="banner-content">
                                        <div class="banner-price-info font-weight-bolder text-white lh-1 ls-25">
                                            40<sup class="font-weight-bold">%</sup><sub
                                                    class="font-weight-bold text-uppercase ls-25">Off</sub>
                                        </div>
                                        <h4
                                                class="banner-subtitle text-white font-weight-bolder text-uppercase mb-0">
                                            Ultimate Sale</h4>
                                    </div>
                                </div>
                            </div>-->
                            <!-- End of Widget Banner -->

                            <div class="widget widget-products">
                                <div class="title-link-wrapper mb-2">
                                    <h4 class="title title-link font-weight-bold">Mais Produtos</h4>
                                </div>

                                <div class="owl-carousel owl-theme owl-nav-top" data-owl-options="{
                                            'nav': true,
                                            'dots': false,
                                            'items': 1,
                                            'margin': 20
                                        }">

                                    <div class="widget-col">
                                        <?php
                                            foreach($produto_categoria_more as $ko => $vo):
                                            $imagem = (!empty($vo->files)) ? explode(',',$vo->files) : null ;
                                            $destaque = (isset($imagem[0])) ? $imagem[0] : null ;
                                        ?>
                                        <div class="product product-widget">
                                            <figure class="product-media">
                                                <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>">
                                                    <img src="<?= (!empty($destaque)) ? PATH_STORE_UPLOAD.$destaque.'&w=100&h=113' : APPPATH.'/images/nophoto.png'?>" alt="Product"
                                                         width="100" height="113" />
                                                </a>
                                            </figure>
                                            <div class="product-details">
                                                <h4 class="product-name">
                                                    <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>"><?= $vo->nome?></a>
                                                </h4>
                                                <div class="ratings-container">
                                                    <?= $vo->categoria?>
                                                </div>
                                                <div class="product-price">R$ <?= number_format($vo->valor,2,",","."); ?></div>
                                            </div>
                                        </div>
                                        <?php endforeach;?>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </aside>
                <!-- End of Sidebar -->
            </div>
        </div>
    </div>
    <!-- End of Page Content -->
</main>
<input type="hidden" value="<?= $produto->id?>" name="id_produto">
<input type="hidden" value="<?= PATH_STORE ?>" name="base_path">
<!-- End of Main -->

<script id="tamanho_variacao" type="text/x-jsrender">
    <label class="mb-1">Tamanho:</label>
    <div class="flex-wrap d-flex align-items-center product-variations">
      {{for tamanhos}}
        <a href='#' class="size tamanho_vars" data-size="{{>tamanho_id}}">{{>nome}}</a>
      {{/for}}
    </div>
</script>

<script id="valor_variacao" type="text/x-jsrender">
   <span>R$ {{>valor}}</span><br>
   <span style='font-size:9px;'>Estoque Disponivel: {{>quantidade}} item(s)</span>
   <input class='estoque_disponivel' value='{{>quantidade}}' type='hidden'>
</script>

<script id="valor_quantidade" type="text/x-jsrender">
    <div class="product-form container">
        <div class="product-qty-form">
            <div class="input-group">
                <input class="quantity form-control" type="number" min="1"
                       max="10000000">
                <button class="quantity-plus w-icon-plus"></button>
                <button class="quantity-minus w-icon-minus"></button>
            </div>
        </div>
    </div>
</script>