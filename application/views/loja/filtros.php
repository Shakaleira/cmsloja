<main class="main">
    <nav class="breadcrumb-nav">
        <div class="container">
            <ul class="breadcrumb bb-no">
                <li><a href="demo1.html">Home</a></li>
                <li><a href="shop-banner-sidebar.html">Busca</a></li>
                <li></li>
            </ul>
        </div>
    </nav>
    <!-- End of Breadcrumb-nav -->

    <div class="page-content mb-10">
        <div class="container">
            <!-- Start of Shop Content -->
            <div class="shop-content row gutter-lg">
              <?php include('inc/filtro_busca_interna.php') ?>
                <!-- End of Shop Sidebar -->
                <!-- Start of Main Content -->
                <div class="main-content">
                    <!-- Start of Shop Banner -->
                    <div class="shop-default-banner shop-boxed-banner banner d-flex align-items-center mb-6 br-xs"
                         style="background-image: url(<?=  PATH_STORE_UPLOAD.$categoria_info->files.'&w=930&h=300' ?>); background-color: <?= isset($configuracoes->cor_primaria) ? $configuracoes->cor_primaria : '#FFC74E' ?> ;">
                        <div class="banner-content">
                            <h4 class="banner-subtitle font-weight-bold"><?= $categoria_info->nome?></h4>
                            <!--<h3 class="banner-title text-white text-uppercase font-weight-bolder ls-10">Smart
                                Watches</h3>-->
                            <a href="<?= PATH_STORE.'/categoria/'.$categoria_info->id.'/busca'?>"
                               class="btn btn-dark btn-rounded btn-icon-right">Ver Todos<i
                                        class="w-icon-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <!-- End of Shop Banner -->

                    <nav class="toolbox sticky-toolbox sticky-content fix-top">
                        <div class="toolbox-left">
                            <a href="#" class="btn btn-primary btn-outline btn-rounded left-sidebar-toggle
                                        btn-icon-left d-block d-lg-none"><i
                                        class="w-icon-category"></i><span>Filters</span></a>
                            <div class="toolbox-item toolbox-sort select-box text-dark">
                                <label>Sort By :</label>
                                <select name="orderby" class="form-control">
                                    <option value="default" selected="selected">Default sorting</option>
                                    <option value="popularity">Sort by popularity</option>
                                    <option value="rating">Sort by average rating</option>
                                    <option value="date">Sort by latest</option>
                                    <option value="price-low">Sort by pric: low to high</option>
                                    <option value="price-high">Sort by price: high to low</option>
                                </select>
                            </div>
                        </div>
                        <div class="toolbox-right">
                            <div class="toolbox-item toolbox-show select-box">
                                <select name="count" class="form-control">
                                    <option value="9">Show 9</option>
                                    <option value="12" selected="selected">Show 12</option>
                                    <option value="24">Show 24</option>
                                    <option value="36">Show 36</option>
                                </select>
                            </div>
                            <div class="toolbox-item toolbox-layout">
                                <a href="shop-boxed-banner.html" class="icon-mode-grid btn-layout active">
                                    <i class="w-icon-grid"></i>
                                </a>
                                <a href="shop-list.html" class="icon-mode-list btn-layout">
                                    <i class="w-icon-list"></i>
                                </a>
                            </div>
                        </div>
                    </nav>
                    <div class="product-wrapper row cols-md-3 cols-sm-2 cols-2">
                        <?php
                          foreach ($produtos as $ko => $vo):
                              $imagem = (!empty($vo->files)) ? explode(',',$vo->files) : null ;
                              $destaque = (isset($imagem[0])) ? $imagem[0] : null ;
                        ?>
                        <div class="product-wrap">
                            <div class="product text-center">
                                <figure class="product-media">
                                    <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>">
                                        <img src="<?= (!empty($destaque)) ? PATH_STORE_UPLOAD.$destaque.'&w=300&h=338&q=90' : APPPATH.'/images/nophoto.png'?>" alt="<?= $vo->nome?>" width="300" height="338" />
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <div class="product-cat">
                                        <a href="#"><?= $vo->categoria?></a>
                                    </div>
                                    <h3 class="product-name">
                                        <a href="#"><?= $vo->nome?></a>
                                    </h3>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 100%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                        <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>" class="rating-reviews"></a>
                                    </div>
                                    <div class="product-pa-wrapper">
                                        <div class="product-price">
                                            <?php
                                                if(!empty($vo->valor) && $vo->valor != 0):
                                            ?>
                                                <ins class="new-price">R$ <?= number_format($vo->valor,2,',','.')?></ins>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="toolbox toolbox-pagination justify-content-between">
                        <p class="showing-info mb-2 mb-sm-0">
                            Showing<span>1-12 of 60</span>Products
                        </p>
                        <ul class="pagination">
                            <li class="prev disabled">
                                <a href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
                                    <i class="w-icon-long-arrow-left"></i>Prev
                                </a>
                            </li>
                            <li class="page-item active">
                                <a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="#">2</a>
                            </li>
                            <li class="next">
                                <a href="#" aria-label="Next">
                                    Next<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End of Main Content -->
            </div>
            <!-- End of Shop Content -->
        </div>
    </div>
</main>
<!-- End of Main -->