<?php
    if(!empty($produtos)):
    foreach($produtos as $k => $v):
        $categoria_foto = (!empty($v['image'])) ? UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$v['image'].'&w=820&h=364' :  UPLOAD_PATH_WITHOUT_THUMB.'categoria_nophoto.png';
?>
<div class="title-link-wrapper mb-3">
    <h2 class="title mb-0 pt-2 pb-2"><?= $v['categoria']?></h2>
</div>
<div class="row grid banner-product-wrapper">
    <div class="grid-item col-xl-5col3 col-lg-3 col-sm-8 col-12">
        <div class="banner banner-fixed br-sm">
            <figure>
                <img src="<?= $categoria_foto ?>" alt="Banner" width="820" height="364" style="background-color: #E4E5E7;">
            </figure>
            <div class="banner-content y-50">
                <!--<h5 class="banner-subtitle text-capitalize font-weight-normal">Get up to <strong class="text-secondary">25% Off</strong></h5>-->
                <h3 class="banner-title text-uppercase"><?= $v['categoria']?></h3>
                <!--<div class="banner-price-info text-dark lh-1 ls-25">Collection</div>-->
                <hr class="banner-divider bg-dark">
                <a href="<?= base_url('categoria/'.$v['categoria_id'])?>" class="btn btn-dark btn-link btn-slide-right btn-icon-right">
                    Ver Todos da Categoria  <i class="w-icon-long-arrow-right mb-1"></i>
                </a>
            </div>
        </div>
    </div>
    <?php
        $list_products =  array_slice($v['items'],0,7);
        foreach($list_products as $ko => $vo):
            $imagem = (!empty($vo->files)) ? explode(',',$vo->files) : null ;
            $destaque = (isset($imagem[0])) ? $imagem[0] : null ;
    ?>
    <div class="grid-item col-xl-5col col-lg-3 col-sm-4 col-6">
        <div class="product product-simple text-center">
            <figure class="product-media">
                <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>">
                    <img src="<?= (!empty($destaque)) ? PATH_STORE_UPLOAD.$destaque.'&w=260&h=291&q=90' : APPPATH.'/images/nophoto.png'?>" alt="<?= $vo->nome?>" >
                </a>
                <div class="product-action-vertical">
                    <!--<a href="#" class="btn-product-icon btn-wishlist w-icon-heart" title="Add to wishlist"></a>
                    <a href="#" class="btn-product-icon btn-compare w-icon-compare" title="Add to Compare"></a>-->
                </div>
                <div class="product-action">
                    <a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>" class="btn-product" title="Visualizar">Visualizar</a>
                </div>
            </figure>
            <div class="product-details">
                <h4 class="product-name"><a href="<?= PATH_STORE.'produto/'.convertUrlFormat($vo->nome).'/'.$vo->id ?>"><?= $vo->nome?></a></h4>
                <div class="product-pa-wrapper">
                    <div class="product-price">
                        <?php
                         if(!empty($vo->valor) && $vo->valor != 0):
                        ?>
                            <ins class="new-price">R$ <?= number_format($vo->valor,2,',','.')?></ins>
                        <?php endif;?>
                    </div>
                </div>
                <div class="sold-by">
                    Cor: <a href="#"><?= $vo->cor?></a>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endforeach; ?>
<?php endif; ?>