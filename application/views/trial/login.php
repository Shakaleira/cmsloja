<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8" />
    <title>Controle de Estoque - Trial</title>

    <meta name="description" content="Sistema de controle de Estoque" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700" />
    <link href="<?= base_url()?>assets/css/demo1/pages/login/login-1.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bodaterangepickerotstrap-touchspin.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/quill/dist/quill.snow.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/dual-listbox/dist/dual-listbox.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/custom/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/custom/vendors/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/custom/vendors/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/demo1/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/demo1/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/demo1/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url()?>assets/css/demo1/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="<?= base_url()?>assets/media/logos/favicon.ico.png" />
</head>

<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"
>
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <!--begin::Aside-->

            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url(<?=base_url()?>assets/media/bg/bg-4.jpg);">
                <div class="kt-grid__item">
                    <a href="#" class="kt-login__logo">
                        <img width="120px" src="<?= base_url()?>images/mini.png" />
                    </a>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--middle">
                        <h3 class="kt-login__title">Bem vindo!</h3>
                        <h4 class="kt-login__subtitle">Controle Seu Estoque</h4>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright">
                            &copy
                            <?= date('Y')?>
                            Dfolio Sistemas
                        </div>

                        <div class="kt-login__menu">
                            <a href="#" class="kt-link">Termos</a>

                            <a href="#" class="kt-link">Duvidas</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--order-tablet-and-mobile-1 kt-login__wrapper">
                <!--begin::Head-->

                <!--end::Head-->

                <!--begin::Body-->

                <div class="kt-login__body">
                    <!--begin::Signin-->

                    <div class="kt-login__form">
                        <div class="kt-login__title">
                            <h3>Faça seu Cadastro</h3>
                        </div>

                        <form class="kt-form">
                            <legend>Dados do Usuario</legend>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Sua Empresa ou Nome" name="nome" autocomplete="off" required />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="CPF/CNPJ" id='cpfcnpj' name="documento" autocomplete="off" required />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="email" placeholder="E-mail" name="email" autocomplete="off" required />
                            </div>
                            <div class="form-group">
                                <input class="form-control telefone" type="text" placeholder="Telefone" name="telefone" required />
                            </div>
                            <hr>
                            <legend>Dados de Login</legend>
                            <div class="form-group">
                                <input id='limit_hold' class="form-control" type="text"  placeholder="Escolha um login curto Ex:meuestoque" name="login" required />
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name='senha' placeholder="Escolha uma Senha" name="telefone" required />
                            </div>
                            <br clear="all">
                            <div class="form-group">
                                <button id="kt_login_signin_submit" class="btn btn-primary btn-elevate kt-login__btn-primary" style="width:100%">Registrar</button>
                            </div><hr>
                            <div class="form-group">
                                 <a href="<?= base_url()?>">
                                    <button type='button' class="btn btn-success btn-elevate " style="width:100%">Fazer Login</button>
                                </a>
                            </div>
                            <input type="hidden" value="1" name="ativo">
                            <input type="hidden" value="1" name="nivel_id">
                            <input type="hidden" value="edit,remove,view" name="permissoes">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var KTAppOptions = {
        colors: {
            state: { brand: "#5d78ff", dark: "#282a3c", light: "#ffffff", primary: "#5867dd", success: "#34bfa3", info: "#36a3f7", warning: "#ffb822", danger: "#fd3995" },
            base: { label: ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"], shape: ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"] },
        },
    };
</script>
<script src="<?= base_url()?>assets/vendors/general/jquery/dist/jquery.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/moment/min/moment.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/wnumb/wNumb.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/bootstrap-switch.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/autosize/dist/autosize.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/dropzone.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/quill/dist/quill.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/@yaireo/tagify/dist/tagify.polyfills.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/@yaireo/tagify/dist/tagify.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/summernote/dist/summernote.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/markdown/lib/markdown.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/bootstrap-markdown.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/bootstrap-notify.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/jquery-validation.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/toastr/build/toastr.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/dual-listbox/dist/dual-listbox.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/raphael/raphael.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/morris.js/morris.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/counterup/jquery.counterup.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/custom/js/vendors/sweetalert2.init.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/vendors/general/dompurify/dist/purify.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/demo1/scripts.bundle.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/jquery.mask.min.js" type="text/javascript"></script>
<script>
    var ambiente = '<?= $actual_link = "http://$_SERVER[HTTP_HOST]";?>';
</script>
<script src="<?= base_url()?>assets/js/global.js" type="text/javascript"></script>
<script src="<?= base_url()?>assets/js/scripts_trial.js" type="text/javascript"></script>
</body>
</html>
