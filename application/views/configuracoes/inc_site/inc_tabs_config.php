<div class="card card-custom">
    <!--begin::Header-->
    <div class="card-header card-header-tabs-line">
        <ul class="nav nav-dark nav-bold nav-tabs nav-tabs-line" data-remember-tab="tab_id" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#kt_builder_page" role="tab">Informações Gerais</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#kt_builder_mobile_header" role="tab">Logomarca e Imagens</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#cores_fontes" role="tab">Cores e Fontes</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#paginas" role="tab">Paginas</a>
            </li>
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content pt-3">
            <div class="tab-pane active" id="kt_builder_page">
                <div class="row">
                    <?php
                        $fields_restrict = [
                            'endereco',
                            'logo',
                            'empresa',
                            'responsavel',
                            'site',
                            'ip_config',
                            'hora_login',
                            'hora_logout',
                            'telefone','email'
                        ];
                        foreach ($conf as $key => $value):
                            if(in_array($key,$fields_restrict)){ continue; }
                            $type = (strpos($key,'data_') !== false) ? 'date' : 'text';
                            $value = format_value($value,$key);
                    ?>
                        <div class="col-xl-6">
                            <div class="form-group">
                                <label><?= strtoupper(str_replace('_',' ',str_replace('_id','',str_replace('boleano','',$key))))?></label>
                                <?php if(strpos($key,'_id') !== false || strpos($key,'boleano') !== false) : ?>
                                    <?= set_combo($key,$value); ?>
                                <?php else: ?>
                                    <input type="<?= $type?>" class="form-control form-control-lg <?= $key?>" name="<?= $key?>" placeholder="Preencha (o) ou (a): <?= $key?>" value="<?= $value ?>">
                                <?php endif; ?>
                                <span class="form-text text-muted">Aqui fica seu: <?= $key?></span>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="tab-pane" id="kt_builder_mobile_header">
                <div class="row">
                    <div class="col-md-12">
                        <h3>Logomarca</h3>
                        <div class="alert alert-info" role="alert">Logomarca com maximo de 2Mb de Tamanho, Dimensões 127 x 64</div>
                        <div class="form-group">
                            <div class="dropzone dropzone-default" id="dropzone_cadastro">
                                <div class="dropzone-msg dz-message needsclick">
                                    <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                    <span class="form-text text-muted">Uma Imagem por vez</span>
                                </div>
                            </div>
                        </div>
                        <input class="input_image" type="hidden" name="logo" value="<?= (isset($conf->logo) && !empty($conf->logo)) ? $conf->logo : '' ?>">
                        <?=  (isset($conf->logo) && !empty($conf->logo)) ? gera_thumb($conf->logo,144,63,null,'logo') : null ?>
                    </div>
                    <hr><br clear="all">
                    <div class="col-md-12">
                        <h3>Slides</h3>
                        <div class="alert alert-info" role="alert">Logomarca com maximo de 2Mb de Tamanho, Dimensões 1003 x 509</div>
                        <div class="form-group">
                            <div class="dropzone dropzone-default" id="dropzone_slides">
                                <div class="dropzone-msg dz-message needsclick">
                                    <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                    <span class="form-text text-muted">Uma Imagem por vez</span>
                                </div>
                            </div>
                        </div>
                        <input class="input_slides" type="hidden" name="slides" value="<?= (isset($conf->slides) && !empty($conf->slides)) ? $conf->slides : '' ?>">
                        <?=  (isset($conf->slides) && !empty($conf->slides)) ? gera_thumb($conf->slides,144,63,null,'slides') : null ?>
                    </div>
                    <hr><br clear="all">
                    <div class="col-md-12">
                        <h3>Banner Central</h3>
                        <div class="alert alert-info" role="alert">Banner de Centro com maximo de 2Mb de Tamanho, Dimensões 421 x 230- Limite de 3 itens</div>
                        <div class="form-group">
                            <div class="dropzone dropzone-default" id="dropzone_banners">
                                <div class="dropzone-msg dz-message needsclick">
                                    <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                    <span class="form-text text-muted">Uma Imagem por vez</span>
                                </div>
                            </div>
                        </div>
                        <input class="input_banners" type="hidden" name="banner" value="<?= (isset($conf->banner) && !empty($conf->banner)) ? $conf->banner : '' ?>">
                        <?=  (isset($conf->banner) && !empty($conf->banner)) ? gera_thumb($conf->banner,144,63,null,'banner') : null ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="cores_fontes">
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">Cor Primaria:</label>
                    <div class="col-lg-9 col-xl-4">
                        <input type='text' class="form-control form-control-lg spectrum_color" name='cor_primaria' value="<?= (isset($conf->cor_primaria) && !empty($conf->cor_primaria)) ? $conf->cor_primaria : '#f44336' ?>" required>
                        <div class="form-text text-muted">Cor Referente a maioria dos elementos do site</div>
                    </div>
                </div>
                <hr>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">Cabecalho:</label>
                    <div class="col-lg-9 col-xl-4">
                        <input type='text' class="form-control form-control-lg spectrum_color" name='color_header' value="<?= (isset($conf->color_header) && !empty($conf->color_header)) ? $conf->color_header : '#f44336' ?>" />
                        <div class="form-text text-muted">Cor do Cabeçalho (header)</div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-lg-3 col-form-label text-lg-right">Cabecalho Topo:</label>
                    <div class="col-lg-9 col-xl-4">
                        <input type='text' class="form-control form-control-lg spectrum_color"  name='color_header_top' value="<?= (isset($conf->color_header_top) && !empty($conf->color_header_top)) ? $conf->color_header_top : '#f44336' ?>" />
                        <div class="form-text text-muted">Cor do Cabeçalho cais acima (header)</div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="paginas">

                <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample3">
                    <div class="card">
                        <div class="card-header" id="headingOne3">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseOne3" aria-expanded="false">A Empresa</div>
                        </div>
                        <div id="collapseOne3" class="collapse" data-parent="#accordionExample3" style="">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Titulo Pagina</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="sobre_titulo" placeholder="Preencha o titulo da pagina" value="<?= isset($conf->sobre_titulo) ? $conf->sobre_titulo : '' ?>">
                                    <span class="form-text text-muted">Referencia de Titulo da pagina Empresa</span>
                                </div>
                                <div class="form-group">
                                    <label>SubTitulo Pagina</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="sobre_subtitulo" placeholder="Preencha o subtitulo da pagina" value="<?= isset($conf->sobre_subtitulo) ? $conf->sobre_subtitulo : '' ?>">
                                    <span class="form-text text-muted">Referencia de SubTitulo da pagina Empresa</span>
                                </div>
                                <div class="form-group">
                                    <label>Titulo Descrição Pagina</label>
                                    <input type="text" class="form-control form-control-solid form-control-lg" name="sobre_titulo_descricao" placeholder="Preencha o Titulo da pagina" value="<?= isset($conf->sobre_titulo_descricao) ? $conf->sobre_titulo_descricao : '' ?>">
                                    <span class="form-text text-muted">Referencia de Titulo Descricao Empresa</span>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-12 col-form-label text-lg-left">Descrição da Empresa</label>
                                    <div class="col-lg-12 col-xl-12">
                                        <textarea name="sobre_descricao" class="form-control form-control-solid form-control-lg summernote"><?= isset($conf->sobre_descricao) ? $conf->sobre_descricao : '' ?></textarea>
                                        <span class="form-text text-muted">Texto Formatado.</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="alert alert-info" role="alert">Banner de Centro com maximo de 2Mb de Tamanho, Dimensões 610 x 230 - 1 Item</div>
                                    <div class="form-group">
                                        <div class="dropzone dropzone-default" id="dropzone_sobre_principal">
                                            <div class="dropzone-msg dz-message needsclick">
                                                <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                                <span class="form-text text-muted">Uma Imagem por vez</span>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="input_principal_sobre" type="hidden" name="input_principal_sobre" value="<?= (isset($conf->input_principal_sobre) && !empty($conf->input_principal_sobre)) ? $conf->input_principal_sobre : '' ?>">
                                    <?=  (isset($conf->input_principal_sobre) && !empty($conf->input_principal_sobre)) ? gera_thumb($conf->input_principal_sobre,80,80,null,'input_principal_sobre') : null ?>
                                </div>
                                <div class="form-group">
                                    <h3>Imagens da Empresa</h3>
                                    <div class="alert alert-info" role="alert">Imagens da Empresa com maximo de 2Mb de Tamanho, Dimensões 295 x 332- Limite de 4 itens</div>
                                    <div class="form-group">
                                        <div class="dropzone dropzone-default" id="dropzone_imagens_empresa">
                                            <div class="dropzone-msg dz-message needsclick">
                                                <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                                <span class="form-text text-muted">Uma Imagem por vez</span>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="input_imagens_empresa" type="hidden" name="imagens_empresa" value="<?= (isset($conf->imagens_empresa) && !empty($conf->imagens_empresa)) ? $conf->imagens_empresa : '' ?>">
                                    <?=  (isset($conf->imagens_empresa) && !empty($conf->imagens_empresa)) ? gera_thumb($conf->imagens_empresa,80,80,null,'imagens_empresa') : null ?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo3">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo3" aria-expanded="false">Accounting Updates</div>
                        </div>
                        <div id="collapseTwo3" class="collapse" data-parent="#accordionExample3" style="">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree3">
                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree3" aria-expanded="false">Latest Payroll</div>
                        </div>
                        <div id="collapseThree3" class="collapse" data-parent="#accordionExample3" style="">
                            <div class="card-body">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--Fim Tabs-->
        </div>
    </div>
    <!-- Referencia e Leitura -->
    <div class="col-md-12">
        <div class="form-group mb-8">
            <div class="alert alert-custom alert-default" role="alert">
                <div class="alert-icon">
                    <span class="svg-icon svg-icon-primary svg-icon-xl">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Tools/Compass.svg-->
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="0" y="0" width="24" height="24"></rect>
                                <path d="M7.07744993,12.3040451 C7.72444571,13.0716094 8.54044565,13.6920474 9.46808594,14.1079953 L5,23 L4.5,18 L7.07744993,12.3040451 Z M14.5865511,14.2597864 C15.5319561,13.9019016 16.375416,13.3366121 17.0614026,12.6194459 L19.5,18 L19,23 L14.5865511,14.2597864 Z M12,3.55271368e-14 C12.8284271,3.53749572e-14 13.5,0.671572875 13.5,1.5 L13.5,4 L10.5,4 L10.5,1.5 C10.5,0.671572875 11.1715729,3.56793164e-14 12,3.55271368e-14 Z" fill="#000000" opacity="0.3"></path>
                                <path d="M12,10 C13.1045695,10 14,9.1045695 14,8 C14,6.8954305 13.1045695,6 12,6 C10.8954305,6 10,6.8954305 10,8 C10,9.1045695 10.8954305,10 12,10 Z M12,13 C9.23857625,13 7,10.7614237 7,8 C7,5.23857625 9.23857625,3 12,3 C14.7614237,3 17,5.23857625 17,8 C17,10.7614237 14.7614237,13 12,13 Z" fill="#000000" fill-rule="nonzero"></path>
                            </g>
                        </svg>
                        <!--end::Svg Icon-->
                    </span>
                </div>
                <div class="alert-text">Meu Link de Exibição é:
                    <code><?= base_url('loja/'.$_SESSION['upload'])?></code> <br> FIQUE ATENTO AO PREENCHIMENTO DOS CAMPOS DESSE SETOR, PARA QUE APAREÇA NO SITE.</div>
            </div>
        </div>
    </div>
</div>
<!--end::Card-->