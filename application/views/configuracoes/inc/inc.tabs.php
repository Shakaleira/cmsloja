<div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
    <div class="wizard-wrapper">
        <div class="wizard-icon">
            <span class="svg-icon svg-icon-2x">
                <!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <polygon points="0 0 24 0 24 24 0 24" />
                        <path d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                        <path d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z" fill="#000000" fill-rule="nonzero" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </div>
        <div class="wizard-label">
            <h3 class="wizard-title">Dados Empresa</h3>
            <div class="wizard-desc">Informações Basicas para o Sistena</div>
        </div>
    </div>
</div>
<!--end::Wizard Step 1 Nav-->
<!--begin::Wizard Step 2 Nav-->
<div class="wizard-step" data-wizard-type="step">
    <div class="wizard-wrapper">
        <div class="wizard-icon">
            <span class="svg-icon svg-icon-2x">
                <!--begin::Svg Icon | path:assets/media/svg/icons/Map/Compass.svg-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24" />
                        <path d="M12,21 C7.02943725,21 3,16.9705627 3,12 C3,7.02943725 7.02943725,3 12,3 C16.9705627,3 21,7.02943725 21,12 C21,16.9705627 16.9705627,21 12,21 Z M14.1654881,7.35483745 L9.61055177,10.3622525 C9.47921741,10.4489666 9.39637436,10.592455 9.38694497,10.7495509 L9.05991526,16.197949 C9.04337012,16.4735952 9.25341309,16.7104632 9.52905936,16.7270083 C9.63705011,16.7334903 9.74423017,16.7047714 9.83451193,16.6451626 L14.3894482,13.6377475 C14.5207826,13.5510334 14.6036256,13.407545 14.613055,13.2504491 L14.9400847,7.80205104 C14.9566299,7.52640477 14.7465869,7.28953682 14.4709406,7.27299168 C14.3629499,7.26650974 14.2557698,7.29522855 14.1654881,7.35483745 Z" fill="#000000" />
                    </g>
                </svg>
                <!--end::Svg Icon-->
            </span>
        </div>
        <div class="wizard-label">
            <h3 class="wizard-title">Site</h3>
            <div class="wizard-desc">Configurações do Site</div>
        </div>
    </div>
</div>
<hr>
<?php
    if(isset($conf->logo) && !empty($conf->logo)):
?>
    <div class="col-md-12" style="position:relative;">
        <center>
            <img src="<?= UPLOAD_PATH.'/'.$_SESSION['upload'].'/'.$conf->logo?>" style="width:80px;" />
            <a href="#" class="delete_logo_conf btn btn-light-danger font-weight-bold mr-2">Deletar Logo</a>
        </center>
    </div>
<?php endif; ?>