<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3"><?= $tipo_comando == 'edit' ? 'Edição de Dados' : 'Novo Cadastro' ?> </h2>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item">
                                <a href="<?= base_url("cadastros/{$modelo}") ?>" class="text-muted">Listagem</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#" class="text-muted"><?= strtoupper($modelo)?></a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-2" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="false">
                            <!--begin: Wizard Nav-->
                           <?php include('inc/tabs.inc.php')?>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->
                            <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                                <!--begin: Wizard Form-->
                                <div class="row">
                                    <div class="offset-xxl-2 col-xxl-8">
                                        <form id="kt_form" class="form form_cadastros" action='#'>
                                            <!--begin: Wizard Step 1-->
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">
                                                <h4 class="mb-10 font-weight-bold text-dark">Caracteristicas do Item</h4>
                                                <!--end::Input-->
                                                <div class="row">
                                                    <?php
                                                        $fields_restrict = array_merge($fields_restrict,['quantidade','descricao','files','data_cadastro','data_inativo']);
                                                        foreach ($item as $key => $value):
                                                            if(in_array($key,$fields_restrict)){ continue; }
                                                            $type = (strpos($key,'data_') !== false) ? 'date' : 'text';
                                                            $value = format_value($value,$key);
                                                    ?>
                                                        <div class="col-xl-6">
                                                            <div class="form-group">
                                                                <label><?= strtoupper(str_replace('_',' ',str_replace('_id','',str_replace('boleano','',$key))))?> <?= (in_array($key,$fields_required)) ? '<span style="color:red">*</span> (Obrigatório)' : null ?></label>
                                                                <?php if(strpos($key,'_id') !== false || strpos($key,'boleano') !== false) : ?>
                                                                    <?= set_combo($key,$value); ?>
                                                                <?php else: ?>
                                                                    <input type="<?= $type?>" class="<?= class_trait($key,$value,$modelo)?> form-control form-control-lg <?= $key?>" name="<?= $key?>" placeholder="Preencha (o) ou (a): <?= $key?>" value="<?= $value ?>" <?= (in_array($key,$fields_required)) ? 'required' : null ?>>
                                                                <?php endif; ?>
                                                                <span class="form-text text-muted">Aqui fica seu: <?= $key?> </span>
                                                            </div>
                                                        </div>
                                                            <div class="render_<?= $key?>"></div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <?php if(isset($item->descricao) || array_key_exists('descricao',$item)): ?>
                                            <div class="pb-5" data-wizard-type="step-content">
                                                <h4 class="mb-10 font-weight-bold text-dark">Descrição</h4>
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="form-group">
                                                            <label>Descrição</label>
                                                            <div class="form-control form-control-solid form-control-lg summernote" id="kt_summernote_1"><?= isset($item->descricao) ? $item->descricao : '' ?></div>
                                                            <span class="form-text text-muted">Texto Formatado.</span>
                                                        </div>
                                                        <input type="hidden" style="display:none;" name="descricao" value='<?= isset($item->descricao) ? $item->descricao : '' ?>'>
                                                        <!--end::Input-->
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                            <?php if(isset($item->files) || array_key_exists('files',$item)): ?>
                                            <div class="pb-5" data-wizard-type="step-content">
                                                <h4 class="mb-10 font-weight-bold text-dark">Upload de Imagens</h4>
                                                <div class="row">
                                                    <div class="col-xl-12">
                                                        <div class="form-group">
                                                            <label>Imagem</label>
                                                            <div class="dropzone dropzone-default" id="dropzone_cadastro">
                                                                <div class="dropzone-msg dz-message needsclick">
                                                                    <h3 class="dropzone-msg-title">Arraste a imagem para cá, ou faça o upload.</h3>
                                                                    <span class="form-text text-muted">Uma Imagem</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12">
                                                        <?php
                                                            if(isset($item->files) && !empty($item->files)):
                                                                if(!empty($item->files) && $tipo_comando == 'edit'):
                                                                    $imagens = explode(',',$item->files);
                                                                    ?>
                                                                    <?php
                                                                    foreach ($imagens as $ch => $vale):
                                                                        ?>
                                                                        <div class='imagem_<?= $ch?>' style="position:relative;float:left;margin: 10px;border: 1px solid #ddd;">
                                                                            <i title='deletar' style="color:red;position:absolute;right:0;cursor:pointer;top:0;" data-posicao='<?= $ch?>' data-produto="<?= $id?>" data-campo_nome='files' class="deletar_imagem flaticon-delete-1"></i>
                                                                            <?= gera_thumb_default($vale,167,155); ?>
                                                                        </div>
                                                                    <?php endforeach; ?>
                                                                    <div class="row carregamento"></div>
                                                                <?php
                                                                endif;
                                                            endif;
                                                        ?>
                                                        <input class="input_image" type="hidden" name="files" value="<?= (isset($item->files) && !empty($item->files)) ? $item->files : '' ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <?php endif;?>
                                            <?php if(isset($modelo) && $modelo == 'produto'): ?>
                                                <div class="pb-5" data-wizard-type="step-content">
                                                    <h4 class="mb-10 font-weight-bold text-dark">Variações de Produto</h4>
                                                    <div class="alert alert-info" role="alert">
                                                        Coloque as variações deste produto, cores, tamanhos e valores que podem existir nesse produto.
                                                    </div>
                                                    <div class="row clone_variation">
                                                        <?php
                                                        foreach ($item as $key => $value):
                                                            if(!in_array($key,['cor_id','tamanho_id','valor','quantidade'])){ continue; }
                                                            $type = (strpos($key,'data_') !== false) ? 'date' : 'text';
                                                            $value = format_value($value,$key);
                                                        ?>
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="col-md-12"><?= strtoupper(str_replace('_',' ',str_replace('_id','',str_replace('boleano','',$key))))?> <?= (in_array($key,$fields_required)) ? '<span style="color:red">*</span> (Obrigatório)' : null ?></label>
                                                                    <?php if(strpos($key,'_id') !== false || strpos($key,'boleano') !== false) : ?>
                                                                        <?= set_combo($key,$value); ?>
                                                                    <?php else: ?>
                                                                        <input type="<?= $type?>" class="<?= class_trait($key,$value,$modelo)?> form-control form-control-lg <?= $key?>" name="<?= $key?>" placeholder="Preencha (o) ou (a): <?= $key?>" value="<?= $value ?>" <?= (in_array($key,$fields_required)) ? 'required' : null ?>>
                                                                    <?php endif; ?>
                                                                </div>
                                                            </div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-md-12" align="right">
                                                             <span class="more_variation svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo3\dist/../src/media/svg/icons\Code\Plus.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                        <circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10"/>
                                                                        <path d="M11,11 L11,7 C11,6.44771525 11.4477153,6 12,6 C12.5522847,6 13,6.44771525 13,7 L13,11 L17,11 C17.5522847,11 18,11.4477153 18,12 C18,12.5522847 17.5522847,13 17,13 L13,13 L13,17 C13,17.5522847 12.5522847,18 12,18 C11.4477153,18 11,17.5522847 11,17 L11,13 L7,13 C6.44771525,13 6,12.5522847 6,12 C6,11.4477153 6.44771525,11 7,11 L11,11 Z" fill="#000000"/>
                                                                    </g>
                                                                </svg><!--end::Svg Icon-->
                                                             </span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                        if(isset($variacoes) && $variacoes):
                                                    ?>
                                                            <?php
                                                                foreach ($variacoes as $k => $v):
                                                            ?>
                                                                    <div class="row variacoes_id_<?= $k ?>">
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label class="col-md-12">Tamanho</label>
                                                                                <select class="select2clone form-control form-control-solid form-control-lg" name="variacao[tamanho_id_variacao][]" disabled>
                                                                                    <option><?= $v->tamanho?></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label class="col-md-12">Quantidade </label>
                                                                                <input type="number" class="form-control form-control-lg " name="variacao[quantidade][]" placeholder="Preencha a quantidade" value="<?= $v->quantidade?>" disabled/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3">
                                                                            <div class="form-group">
                                                                                <label class="col-md-12">Cor</label>
                                                                                <select class="select2clone form-control form-control-solid form-control-lg" name="variacao[cor_id_variacao][]" disabled>
                                                                                    <option><?= $v->nome_referencia?></option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label class="col-md-12">VALOR </label>
                                                                                <input type="text" class="form-control form-control-lg valor" name="variacao[valor][]" placeholder="Preencha (o) ou (a): valor" value="<?= $v->valor?>" disabled/>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-1">
                                                                            <div class="form-group">
                                                                                <label class="col-md-12"></label>
                                                                                <span style='cursor:pointer'  data-id='<?= $v->id?>' class="remove_variacao svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo3\dist/../src/media/svg/icons\General\Trash.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                                        <rect x="0" y="0" width="24" height="24"/>
                                                                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
                                                                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                                                                                    </g>
                                                                                </svg><!--end::Svg Icon-->
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                            <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <div class="base_clone_variations"></div>
                                                </div>
                                            <?php endif;?>
                                            <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                <div class="mr-2">
                                                    <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Anterior</button>
                                                </div>
                                                <div>
                                                    <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Salvar</button>
                                                    <button type="button" class="btn btn-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-next">Proximo</button>
                                                </div>
                                            </div>
                                            <?php
                                                if($tipo_comando == 'edit'):
                                            ?>
                                                <input type="hidden" name="id" value='<?= $id ?>'>
                                            <?php endif; ?>
                                            <input type="hidden" name="base" value='cadastros' disabled>
                                            <input type="hidden" name="modelo" value='<?= $modelo ?>' disabled>
                                            <input type="hidden" name="comando" value='<?= $tipo_comando ?>' disabled>
                                        </form>
                                    </div>
                                    <!--end: Wizard-->
                                </div>
                                <!--end: Wizard Form-->
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
</div>
<script id="variacoes_produto" type="text/x-jsrender">
<div class="row itens_clonados">
    <div class="col-md-2">
        <div class="form-group">
            <label class="col-md-12">Tamanho</label>
            <select class="select2clone form-control form-control-solid form-control-lg" name="variacao[tamanho_id_variacao][]" required>
                <option></option>
                {{for tamanhos}}
                  <option value='{{>id}}'>{{>nome}}</option>
                {{/for}}
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="col-md-12">Qtd.: </label>
            <input type="number" class="form-control form-control-lg" name="variacao[quantidade][]" placeholder="Estoque" value="1" />
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="col-md-12">Cor</label>
            <select class="select2clone form-control form-control-solid form-control-lg" name="variacao[cor_id_variacao][]" required>
                <option></option>
                {{for cores}}
                  <option value='{{>id}}'>{{>nome_referencia}}</option>
                {{/for}}
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label class="col-md-12">VALOR </label>
            <input type="text" class="form-control form-control-lg valor" name="variacao[valor][]" placeholder="Preencha (o) ou (a): valor" value="" />
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group">
             <label class="col-md-12"></label>
             <span style='cursor:pointer'  class="remove_cloned_item svg-icon svg-icon-primary svg-icon-2x"><!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo3\dist/../src/media/svg/icons\General\Trash.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                    <rect x="0" y="0" width="24" height="24"/>
                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                </g>
            </svg><!--end::Svg Icon-->
            </span>
        </div>
    </div>
</div>
</script>