<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-3 py-lg-8 subheader-transparent" id="kt_subheader">
            <div class="container d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h2 class="d-flex align-items-center text-dark font-weight-bold my-1 mr-3"><?= $tipo_comando == 'edit' ? 'Edição de Dados' : 'Novo Cadastro' ?></h2>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold my-2 p-0">
                            <li class="breadcrumb-item">
                                <a href="<?= base_url("cadastros/{$modelo}") ?>" class="text-muted">Cadastros</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-2" id="kt_wizard" data-wizard-state="step-first" data-wizard-clickable="false">
                            <!--begin: Wizard Body-->
                            <div class="wizard-body py-8 px-8 py-lg-20 px-lg-10">
                                <!--begin: Wizard Form-->
                                <div class="row">
                                    <div class="offset-xxl-2 col-xxl-8">
                                        <form id="kt_form" class="form form_cadastros" action='#'>
                                            <!--begin: Wizard Step 1-->
                                            <div class="pb-5" data-wizard-type="step-content" data-wizard-state="current">

                                                <h4 class="mb-10 font-weight-bold text-dark">Caracteristicas do Item</h4>

                                                <?php
                                                    if(isset($item['status_id']) && $item['status_id'] == 1):
                                                ?>
                                                    <div class="alert alert-warning" role="alert">Status em Condicional</div>
                                                <?php
                                                    elseif ($item['status_id'] == 2):
                                                ?>
                                                    <div class="alert alert-danger" role="alert">Status em Cancelado - Após o Cancelamento nenhuma mudança pode ser feita, o estoque é revalidado de acordo com os itens selecionados.</div>
                                                <?php
                                                    elseif ($item['status_id'] == 3):
                                                ?>
                                                    <div class="alert alert-primary" role="alert">Status Negocio Fechado</div>
                                                <?php
                                                     elseif ($item['status_id'] == 5):
                                                ?>
                                                    <div class="alert alert-warning" role="alert">Status Solicitção de Orçamento</div>
                                                <?php
                                                    endif;
                                                ?>
                                                <!--end::Input-->
                                                <div class="row">
                                                    <?php
                                                        $fields_restrict = array_merge($fields_restrict,['descricao','files','data_cadastro','data_inativo']);
                                                        foreach ($item as $key => $value):
                                                            if(in_array($key,$fields_restrict)){ continue; }
                                                            $type = (strpos($key,'data_') !== false) ? 'date' : 'text';
                                                            $value = format_value($value,$key);
                                                            ?>
                                                            <div class="col-xl-6">
                                                                <div class="form-group">
                                                                    <label><?= strtoupper(str_replace('_',' ',str_replace('_id','',str_replace('boleano','',$key))))?></label>
                                                                    <?php if(strpos($key,'_id') !== false || strpos($key,'boleano') !== false) : ?>
                                                                        <?= set_combo($key,$value); ?>
                                                                    <?php else: ?>
                                                                        <input type="<?= $type?>" class="form-control form-control-solid form-control-lg <?= $key?>" name="<?= $key?>" placeholder="Preencha (o) ou (a): <?= $key?>" value="<?= $value ?>">
                                                                    <?php endif; ?>
                                                                    <span class="form-text text-muted">Aqui fica seu: <?= $key?></span>
                                                                </div>
                                                            </div>
                                                            <div class="render_<?= $key?>"></div>
                                                        <?php endforeach; ?>
                                                </div>
                                            </div>
                                            <div class="d-flex justify-content-between border-top mt-5 pt-10">
                                                <div class="mr-2">
                                                    <button type="button" class="btn btn-light-primary font-weight-bolder text-uppercase px-9 py-4" data-wizard-type="action-prev">Anterior</button>
                                                </div>
                                                <?php
                                                    if( $item['status_id'] != 2):
                                                ?>
                                                    <div>
                                                        <button type="submit" class="btn btn-success font-weight-bolder text-uppercase px-9 py-4">Salvar</button>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <!--end: Wizard Actions-->
                                            <?php
                                                if($tipo_comando == 'edit'):
                                                    ?>
                                                    <input type="hidden" name="id" value='<?= $id ?>'>
                                                <?php endif; ?>
                                            <input type="hidden" name="base" value='cadastros' disabled>
                                            <input type="hidden" name="modelo" value='<?= $modelo ?>' disabled>
                                            <input type="hidden" name="comando" value='<?= $tipo_comando ?>' disabled>
                                        </form>
                                    </div>
                                    <!--end: Wizard-->
                                </div>
                                <!--end: Wizard Form-->
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
</div>
<script id="estoque" type="text/x-jsrender">
    <div style='background:#ddd;margin:10px 0 0 0;padding:17px' class='id_produto_{{:id}}'>
        <div class='row'>
            <div class="col-md-12">
               <h5>Relação de Itens Selecionados</h5>
            </div>
        </div>
        <div class="row">
           <div class="col-md-1">
                <div class="form-group">
                  <label>Imagem:</label>
                  <img style='width:38px;height:38px;display:block;' src='{{:imagem}}'>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                     <label>Produto:</label>
                     <input type="text" class="form-control"  value='{{:nome}}' placeholder="{{:nome}}" readonly>
                     <input type="hidden" class="form-control" name="orcamento[produto][]" value='{{:id}}'>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                 <label>Cor:</label>
                     <input type="text" class="form-control"  value="{{:cor}}" readonly>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                 <label>Tamanho:</label>
                     <input type="text" class="form-control"  value="{{:tamanho}}" readonly>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                 <label>Quantidade:</label>
                     <input type="number" class="form-control" name="orcamento[quantidade][]" placeholder="Preencha (o) ou (a):" min='1'  value="{{:quantidade}}">
                     <span class="form-text text-muted"><b>Estoque Disponivel: {{:estoque}} (Itens que não estao em condicional)</b></span>
                </div>
            </div>
            {{if status_id != 2}}
            <div class="col-md-1">
                <div class="form-group">
                  <label>Remover:</label><br clear='all'>
                  <center>
                  <span style='cursor:pointer;' data-quantidade='{{:quantidade}}' data-id='{{:id}}' class="deletar_registro svg-icon svg-icon-primary svg-icon-2x">
                  <!--begin::Svg Icon | path:C:\wamp64\www\keenthemes\themes\metronic\theme\html\demo3\dist/../src/media/svg/icons\General\Trash.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>
                            <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                  </span>
                  </center>
                </div>
            </div>
            {{/if}}
        </div>
    </div>
</script>